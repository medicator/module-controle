FROM archlinux

# Installer les dépendances
RUN pacman -Sy --noconfirm \
    arm-none-eabi-gcc \
    arm-none-eabi-newlib \
    make \
    git \
    perl-archive-zip \
    vim \
    jq \
    which \
    && pacman -Syu --noconfirm \
    && pacman -Scc --noconfirm

RUN ln -sf /usr/bin/vendor_perl/crc32 /usr/bin/crc32

# Télécharger le firmware
RUN git clone --recursive --branch v1.5.2 --depth 1 https://github.com/particle-iot/device-os.git firmware && \
    cd firmware

# Lier /bin/sh à /bin/bash
RUN ln -sf /bin/bash /bin/sh

# Utiliser bash comme point d'entrée
ENTRYPOINT ["/bin/bash", "-c"]
