#!/bin/bash

# Arrêter le script dès la première erreur qui survient.
# set -e

# Obtenir la version de deviceOS configurée.
DEVICE_OS_VERSION=`jq -r '.["particle.firmwareVersion"]' .vscode/settings.json`

# Configurer les variables globales
export APPDIR=${APPDIR:-$PWD}
export DEVICE_OS_PATH=${DEVICE_OS_PATH:-"$HOME/.particle/toolchains/deviceOS/$DEVICE_OS_VERSION"}
export PLATFORM=${PLATFORM:-`jq -r '.["particle.targetPlatform"]' .vscode/settings.json`}

# Aller dans le dossier de deviceOS
cd "$DEVICE_OS_PATH/main"

# Compiler le projet
make all --quiet
