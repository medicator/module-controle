#!/bin/bash

# Arrêter le script dès la première erreur qui survient.
set -e

# Obtenir le chemin du script de compilation.
SCRIPT_COMPILATION=`realpath $(dirname $0)/compile.sh`

# Obtenir la liste de tous les fichiers d'exemples à tester.
EXEMPLES=`git ls-files | grep 'examples/.*\.cpp'`

# Créer un dossier temporaire.
TEMP=`mktemp -d`

# S'assurer que le dossier temporaire sera supprimer à la fin du script.
# Ceci sera le cas même s'il rencontre une erreur.
function fin {
  rm -rf $TEMP
}
trap fin EXIT

# Copier les fichiers source du projet dans le dossier temporaire
cp -r src lib project.properties $TEMP

# Pour chaque exemple
for exemple in $EXEMPLES; do
    # Afficher une bannière
    echo -e "\033[34m\033[1m"
    printf %${COLUMNS}s|tr \  =
    echo; echo $exemple
    printf %${COLUMNS}s|tr \  =
    echo -e "\033[0m"

    # Remplacer le fichier principal du projet par l'exemple.
    cp $exemple $TEMP/src/main.cpp

    # Compiler l'exemple.
    APPDIR=$TEMP $SCRIPT_COMPILATION
done
