/**
 * @file TCommunication.h
 * @brief Déclaration de TCommunication
 */

#ifndef TCommunication_H
#define TCommunication_H

#include <TModuleDistribution.h>
#include <Wire.h>
#include <stdint.h>

//! Taille du tampon I²C de réception.
#define TAILLE_TAMPON_I2C 32

//! Gère la communication avec le module de contrôle
class TCommunication {
private:
    //! Instance unique de cette classe.
    static TCommunication *FInstance;

    //! Port I²C à utiliser.
    TwoWire &FPortI2C;

    //! Adresse I²C de ce module lorsqu'il est configuré en mode esclave.
    uint8_t FAdresseEsclave;

    //! Module de distribution.
    TModuleDistribution &FModuleDistribution;

    //! Tampon utilisé en mode maître pour lire les données reçues du module de
    //! contrôle.
    uint8_t FTamponReceptionMaitre[TAILLE_TAMPON_I2C];

    //! Tampon utilisé en mode esclave pour recevoir les données du module de
    //! contrôle.
    uint8_t FTamponReceptionEsclave[TAILLE_TAMPON_I2C];

    //! Tampon utilisé en mode esclave pour envoyer des données au module de
    //! contrôle.
    uint8_t FTamponEnvoiEsclave[TAILLE_TAMPON_I2C];

    int EnvoyerCommande(uint8_t codeCommande, uint8_t *donnees = nullptr,
                        uint8_t tailleDonnees = 0);

    static void OnReceive(int nbOctets);
    static void OnRequest();

    void GererCommande();

    TCommunication(TwoWire &portI2C, uint8_t adresseEsclave,
                   TModuleDistribution &moduleDistribution);

public:
    static TCommunication &Instance(TwoWire &portI2C, uint8_t adresseEsclave,
                                    TModuleDistribution &moduleDistribution);

    void begin();

    int ChangerNbMedicaments(uint8_t nbMedicaments);
    int ObtenirNbMedicaments(uint8_t &nbMedicaments);
    int DeverrouillerRecharge(uint8_t secondes);
};

#endif /* TCommunication_H */
