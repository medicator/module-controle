/**
 * @file TCommunication.cpp
 * @brief Implémentation de TCommunication
 */

#include "TCommunication.h"

//! Adresse I²C du module de contrôle.
#define ADRESSE_MODULE_DISTRIBUTION 10

//! Taille maximale des données dans une commande.
#define TAILLE_DONNEES_MAX 30

//! Code pour changer le nombre de médicaments
//! à distribuer du module de distribution.
#define CODE_CMD_CHANGER_NB_MEDICAMENTS 0
//! Code pour demander au module de distribution combien
//! de médicaments il lui reste à distribuer.
#define CODE_CMD_OBTENIR_NB_MEDICAMENTS 1
//! Code pour demander au module de distribution de
//! déverrouiller la recharge pendant un certain temps.
#define CODE_CMD_DEVERROUILLER_RECHARGE 2

//! Code de la commande d'initialisation.
#define CODE_CMD_INIT 0xff
//! Code de la commande pour envoyer la notification
//! d'un niveau de médicament critique
#define CODE_CMD_NOTIF_NIVEAU 0xfe
//! Code de la commande
#define CODE_CMD_NOTIF_DISTRIBUTION 0xfd

//! Délai en millisecondes entre les tentatives d'initialisation de la
//! communication avec le module de contrôle.
#define DELAI_ESSAI_INIT 100

// Initialiser les variables statiques de TCommunication.
TCommunication *TCommunication::FInstance = nullptr;

/**
 * @brief Initialise un gestionnaire de communication sur le port I²C donné qui
 * contrôle le gestionnaire de distribution et le verrou donné.
 * @note Il faut obligatoirement appelé begin avant n'importe
 * quelle autre méthode de cette classe
 *
 */
TCommunication::TCommunication(
    //! Port I²C.
    TwoWire &portI2C,
    //! Adresse I²C lorsque configuré en mode esclave.
    uint8_t adresseEsclave,
    //! Module de distribution.
    TModuleDistribution &moduleDistribution) :
    FPortI2C(portI2C),
    FAdresseEsclave(adresseEsclave), FModuleDistribution(moduleDistribution) {
    // Enregistrer un code d'erreur dans le tampon de réponse aux requêtes.
    FInstance->FTamponReceptionEsclave[0] = 1;
}

/**
 * @brief Retourne une référence vers l'instance de cette classe si elle est
 * déjà créée, sinon l'instance est créée et retournée.
 *
 * @return L'instance unique de cette classe
 */
TCommunication &TCommunication::Instance(
    //! Port I²C.
    TwoWire &portI2C,
    //! Adresse I²C lorsque configuré en mode esclave.
    uint8_t adresseEsclave,
    //! Module de distribution.
    TModuleDistribution &moduleDistribution) {
    if (FInstance == nullptr) {
        FInstance =
            new TCommunication(portI2C, adresseEsclave, moduleDistribution);
    }
    return *FInstance;
}

/**
 * @brief Initialise le port I²C et la communication avec le module de contrôle.
 *
 */
void TCommunication::begin() {
    FPortI2C.begin(FAdresseEsclave);
    FPortI2C.onReceive(OnReceive);
    FPortI2C.onRequest(OnRequest);
}

/**
 * @brief Change le nombre de médicaments à distribuer du module de distribution
 * pour celui donné
 * @note La valeur de `0` peut être donnée à `nbMedicaments` pour faire en sorte
 * que le module de distribution arrête de distribuer des médicaments.
 *
 * @return Un code d'erreur
 * @retval -1 Le pointeur de données est nul alors que la taille des données est
 * supérieure à 0
 * @retval -2 La quantité de données à envoyer est plus grande que la quantité
 * maximale
 * @retval -3 Erreur de transmission
 * @retval -4 Erreur de réception
 * @retval 0 Aucune erreur
 * @retval 1 Le message reçu par le module de distribution était corrompu
 */
int TCommunication::ChangerNbMedicaments(uint8_t nbMedicaments) {
    return EnvoyerCommande(CODE_CMD_CHANGER_NB_MEDICAMENTS, &nbMedicaments, 1);
}

/**
 * @brief Obtient le nombre de médicaments à distribuer du module de
 * distribution et l'enregistre dans la variable donnée.
 *
 * @return Un code d'erreur
 * @retval -1 Le pointeur de données est nul alors que la taille des données est
 * supérieure à 0
 * @retval -2 La quantité de données à envoyer est plus grande que la quantité
 * maximale
 * @retval -3 Erreur de transmission
 * @retval -4 Erreur de réception
 * @retval 0 Aucune erreur
 * @retval 1 Le message reçu par le module de distribution était corrompu
 */
int TCommunication::ObtenirNbMedicaments(uint8_t &nbMedicaments) {
    int codeErreur = EnvoyerCommande(CODE_CMD_OBTENIR_NB_MEDICAMENTS);
    if (codeErreur == 0) {
        nbMedicaments = FTamponReceptionMaitre[1];
    }
    return codeErreur;
}

/**
 * @brief Déverrouille la recharge de médicaments du module de distribution
 * pendant la durée en secondes données.
 * @note Si `secondes` est de 0, la recharge du module de distribution sera
 * immédiatement reverrouillée
 *
 * @return Un code d'erreur
 * @retval -1 Le pointeur de données est nul alors que la taille des données est
 * supérieure à 0
 * @retval -2 La quantité de données à envoyer est plus grande que la quantité
 * maximale
 * @retval -3 Erreur de transmission
 * @retval -4 Erreur de réception
 * @retval 0 Aucune erreur
 * @retval 1 Le message reçu par le module de distribution était corrompu
 */
int TCommunication::DeverrouillerRecharge(uint8_t secondes) {
    return EnvoyerCommande(CODE_CMD_DEVERROUILLER_RECHARGE, &secondes, 1);
}

/**
 * @brief Envoi la commande donnée
 *
 * @return Un code d'erreur
 * @retval -1 Le pointeur de données est nul alors que la taille des données est
 * supérieure à 0
 * @retval -2 La quantité de données à envoyer est plus grande que la quantité
 * maximale
 * @retval -3 Erreur de transmission
 * @retval -4 Erreur de réception
 * @retval >=0 Code d'erreur retourné dans le premier octet de la réponse du
 * module de contrôle
 */
int TCommunication::EnvoyerCommande(
    //! Code de la commande à envoyer
    uint8_t codeCommande,
    //! Données à envoyer avec la commande.
    //! Ceci peut être nul, si la taille des données est 0.
    uint8_t *donnees,
    //! Taille des données à envoyer.
    uint8_t tailleDonnees) {
    int codeErreur = 0;

    // Si les paramètres sont valides.
    if (donnees == nullptr && tailleDonnees != 0) {
        codeErreur = -1;
    } else if (tailleDonnees > TAILLE_DONNEES_MAX) {
        codeErreur = -2;
    } else {
        // Configurer le port en mode maître.
        FPortI2C.begin();

        Wire.beginTransmission(ADRESSE_MODULE_DISTRIBUTION);

        uint8_t octetIntegrite = codeCommande;

        // Envoyer le code de la commande.
        Wire.write(codeCommande);

        // Envoyer les données.
        for (size_t i = 0; i < tailleDonnees; i++) {
            uint8_t octet = donnees[i];
            octetIntegrite += octet;
            Wire.write(octet);
        }

        // Envoyer des 0 pour le reste de la section de la
        // trame dédiée aux données.
        size_t donneesRestantes = TAILLE_DONNEES_MAX - tailleDonnees;
        for (size_t i = 0; i < donneesRestantes; i++) {
            Wire.write(0);
        }

        // Envoyer l'octet d'intégrité.
        Wire.write(octetIntegrite);

        // Terminer l'envoi tout en gardant le contrôle de la ligne
        // et vérifier le résultat.
        if (Wire.endTransmission(false) == 0) {
            // Lire la réponse.
            if (Wire.requestFrom(ADRESSE_MODULE_DISTRIBUTION,
                                 TAILLE_TAMPON_I2C) == TAILLE_TAMPON_I2C) {
                for (size_t i = 0; i < TAILLE_TAMPON_I2C; i++) {
                    FTamponReceptionMaitre[i] = Wire.read();
                }

                // Utiliser le code d'erreur de la réponse comme code d'erreur.
                codeErreur = FTamponReceptionMaitre[0];
            } else {
                codeErreur = -4;
            }
        } else {
            codeErreur = -3;
        }

        // Configurer le port en mode esclave.
        FPortI2C.begin(FAdresseEsclave);
        FPortI2C.onReceive(OnReceive);
        FPortI2C.onRequest(OnRequest);
    }

    return codeErreur;
}

/**
 * @brief Reçoit une commande du module de contrôle et prépare la réponse à
 * envoyer dans OnRequest.
 *
 */
void TCommunication::OnReceive(
    //! Nombre d'octets reçus.
    int nbOctets) {
    if (FInstance != nullptr) {
        TwoWire &portI2C = FInstance->FPortI2C;
        if (nbOctets == TAILLE_TAMPON_I2C) {
            uint8_t integriteCalculee = 0;

            for (size_t i = 0; i < (TAILLE_TAMPON_I2C - 1); i++) {
                uint8_t octet = portI2C.read();
                integriteCalculee += octet;
                FInstance->FTamponReceptionEsclave[i] = octet;
            }

            uint8_t integriteRecue = portI2C.read();
            FInstance->FTamponReceptionEsclave[TAILLE_TAMPON_I2C - 1] =
                integriteRecue;
            if (integriteCalculee == integriteRecue) {
                FInstance->GererCommande();
            } else {
                // Retourner l'erreur de message corrompu.
                FInstance->FTamponEnvoiEsclave[0] = 1;
            }
        } else {
            // Retourner l'erreur de message corrompu.
            FInstance->FTamponEnvoiEsclave[0] = 1;
        }
    }
}

/**
 * @brief Répond à une demande de données du module de contrôle lorsque ce
 * module de distribution est configuré en mode d'esclave.
 *
 */
void TCommunication::OnRequest() {
    // Effacer les données dans le tampon d'envoi qui n'ont
    // pas été lues par le maître.
    FInstance->FPortI2C.flush();

    if (FInstance != nullptr) {
        // Répondre à la commande reçue.
        FInstance->FPortI2C.write(FInstance->FTamponEnvoiEsclave,
                                  TAILLE_TAMPON_I2C);

        // Retourner le code d'erreur 1 au maître la prochaine fois qu'il lit
        // des données .
        FInstance->FTamponReceptionEsclave[0] = 1;
    }
}

/**
 * @brief Prépare la réponse à une commande du module de contrôle.
 * @note La commande du module de contrôle doit se trouver dans
 * FTamponReceptionEsclave et la réponse se trouvera dans FTamponEnvoiEsclave
 *
 */
void TCommunication::GererCommande() {
    uint8_t codeErreur = 0;

    uint8_t codeCommande = FTamponReceptionEsclave[0];
    if (codeCommande == CODE_CMD_INIT) {
        // Envoyer le nombre de médicaments à distribuer au module de
        // distribution.
        FTamponEnvoiEsclave[1] = FModuleDistribution.MedicADistribuer();
    } else if (codeCommande == CODE_CMD_NOTIF_NIVEAU) {
        // Enregistrer le niveau de médicaments.
        FModuleDistribution.NiveauMedicCritique(FTamponReceptionEsclave[1] !=
                                                0);
    } else if (codeCommande == CODE_CMD_NOTIF_DISTRIBUTION) {
        if (!FModuleDistribution.DistribuerMedic()) {
            // Refuser qu'un médicament soit distribué.
            codeErreur = 3;
        }
    } else {
        // Signaler que la commande est inconnue.
        codeErreur = 2;
    }

    FTamponEnvoiEsclave[0] = codeErreur;
}
