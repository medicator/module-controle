/**
 * @file test.cpp
 * @brief Programme de test du contrôle du buzzer par PWM
 */

#include <TBuzzer.h>

// Ne pas attendre une connexion au réseau avant l'exécution de `void setup()`.
SYSTEM_THREAD(ENABLED);

// Ne pas se connecter automatiquement au réseau cellulaire.
SYSTEM_MODE(MANUAL);

//! Broche du buzzer.
#define BROCHE_BUZZER D2
//! Broche du potentiomètre.
#define BROCHE_POTENTIOMETRE A0
//! Délai de clignottement en ms.
#define DELAI_CLIGNOTTEMENT 500

void setup() {
    Serial.begin(9600);
    TBuzzer::begin(BROCHE_BUZZER, BROCHE_POTENTIOMETRE, DELAI_CLIGNOTTEMENT);
    TBuzzer::Etat(TBuzzer::CLIGNOTTE);
}

void loop() {
    if (Serial.available() > 0) {
        if (Serial.readStringUntil('\n').toInt() == 0) {
            TBuzzer::Etat(TBuzzer::ETEINT);
        } else {
            TBuzzer::Etat(TBuzzer::CLIGNOTTE);
        }
    }
    Serial.print(TBuzzer::Etat() ? "Clignotte" : "Éteint");
    Serial.print(": ");
    Serial.println(TBuzzer::Volume());

    delay(250);
}
