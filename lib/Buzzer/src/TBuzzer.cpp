/**
 * @file TBuzzer.cpp
 * @brief Implémentation de TBuzzer
 *
 */

#include "TBuzzer.h"

//! Valeur de PWM pour éteindre le buzzer
#define PWM_BUZZER_ETEINT 0
//! Fréquence du PWM
#define FREQUENCE_PWM 100000 // 100kHz

// Initialiser les variables statiques de TBuzzer
int TBuzzer::FBuzzer = -1;
int TBuzzer::FPotentiometre = -1;
bool TBuzzer::FEtat = TBuzzer::ETEINT;
Timer TBuzzer::FTimer(1000, TBuzzer::ISR);
bool TBuzzer::FProchainNiveau = false;

/**
 * @brief ISR du timer utilisé pour faire clignotter le buzzer.
 *
 */
void TBuzzer::ISR() {
    if (FEtat == CLIGNOTTE) {
        // Déterminer le volume du buzzer.
        uint8_t volumeBuzzer = FProchainNiveau ? Volume() : PWM_BUZZER_ETEINT;

        // Changer le volume du buzzer.
        analogWrite(FBuzzer, volumeBuzzer, FREQUENCE_PWM);

        // Faire en sorte d'inverser l'état du buzzer la prochaine fois que
        // cette fonction est appelée.
        FProchainNiveau = !FProchainNiveau;
    }
}

/**
 * @brief Configure le buzzer et l'éteint
 * @note Cette méthode doit être appelée avant n'importe quelle autre méthode,
 * sinon le comportement lorsque vous appelerez ces méthodes sera indéfini.
 * @note Vous pouvez appeler cette méthode autant de fois que vous voulez pour
 * reconfigurer le buzzer.
 *
 */
void TBuzzer::begin(
    //! Broche pour contrôler le buzzer
    int brocheBuzzer,
    //! Broche pour lire la valeur du potentiomètre
    int brochePotentiometre,
    //! Délai en ms pendant lequel le buzzer est allumé et éteint lorsqu'il
    //! clignotte
    uint16_t delaiClignottement) {
    // Enregistrer les broches données.
    FBuzzer = brocheBuzzer;
    FPotentiometre = brochePotentiometre;

    // Configurer les broches données.
    pinMode(FBuzzer, OUTPUT);
    pinMode(FPotentiometre, INPUT);

    // Changer la période du timer.
    FTimer.changePeriod(delaiClignottement);

    // Éteindre le buzzer.
    analogWrite(FBuzzer, PWM_BUZZER_ETEINT, FREQUENCE_PWM);
    Etat(ETEINT);
}

/**
 * @brief Permet de faire clignotter ou d'éteindre le buzzer.
 *
 */
void TBuzzer::Etat(
    //! Nouvel état du buzzer.
    //! @see TBuzzer::CLIGNOTTE
    //! @see TBuzzer::ETEINT
    bool etat) {

    // Si l'état a changé.
    if (etat != FEtat) {
        // Changer l'état.
        FEtat = etat;

        // Initialiser un volume au volume éteint.
        uint8_t volume = PWM_BUZZER_ETEINT;

        // S'il faut éteindre le buzzer
        if (etat == ETEINT) {
            // Arrêter le timer.
            FTimer.stop();
        }
        // Sinon, allumer immédiatement le buzzer et démarrer le timer.
        else {
            // Allumer le buzzer.
            volume = Volume();

            // Démarrer le timer.
            FTimer.start();
            FProchainNiveau = false;
        }

        // Changer le volume du buzzer.
        analogWrite(FBuzzer, volume, FREQUENCE_PWM);
    }
}

/**
 * @brief Retourne l'etat du buzzer, s'il est en train de clignotter ou non.
 *
 * @return l'état du buzzer, @see TBuzzer::CLIGNOTTE, @see TBuzzer::ETEINT
 */
bool TBuzzer::Etat() {
    return FEtat;
}

/**
 * @brief Retourne le volume auquel est configuré le buzzer avec le
 * potentiomètre.
 *
 * @return uint8_t Le volume auquel est configuré le buzzer avec le
 * potentiomètre.
 */
uint8_t TBuzzer::Volume() {
    // Lire la valeur du potentiomètre et la convertir d'une résolution de 12
    // bits à une résolution de 8 bits.
    return analogRead(FPotentiometre) >> 4;
}
