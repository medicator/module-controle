/**
 * @file TBuzzer.h
 * @brief Déclaration de TBuzzer
 */

#ifndef TBuzzer_H
#define TBuzzer_H

#include <Particle.h> // Timer

//! Permet de faire clignotter ou non un buzzer et de contrôler
//! son volume par PWM et commander ce PWM à l'aide d'un potentiomètre
class TBuzzer {
private:
    //! Broche pour contrôler le buzzer.
    static int FBuzzer;
    //! Broche pour lire la valeur du potentiomètre.
    static int FPotentiometre;

    //! État du buzzer. Allumé ou éteint.
    static bool FEtat;

    //! Timer pour faire clignotter le potentiomètre.
    static Timer FTimer;

    //! Prochain niveau du buzzer.
    //! Cette variable est constamment inversée par le
    //! ISR afin de faire clignotter le buzzer.
    static bool FProchainNiveau;

    // Supprimer le constructeur.
    TBuzzer() = delete;

    static void ISR();

public:
    //! Valeur à passer à Etat pour allumer le buzzer.
    static const bool CLIGNOTTE = true;
    //! Valeur à passer à Etat pour éteindre le buzzer.
    static const bool ETEINT = false;

    static void begin(int brocheBuzzer, int brochePotentiometre,
                      uint16_t delaiClignottement);

    static void Etat(bool etat);
    static bool Etat();

    static uint8_t Volume();
};

#endif /* TBuzzer_H */
