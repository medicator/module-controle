/**
 * @file TConnexionCellulaire.h
 * @brief Déclaration de TConnexionCellulaire
 */

#ifndef TConnexionCellulaire_H
#define TConnexionCellulaire_H

#include <TDelClignotteTemp.h>
#include <time.h> // time_t

//! S'assure de la connexion au réseau cellulaire
class TConnexionCellulaire {
private:
    //! DEL pour signaler une erreur.
    TRefDel<TDelClignotteTemp> FDelErreur;

    //! Heure de la dernière synchronisation de l'heure
    time_t FDerniereSyncHeure;

public:
    //! Interval de synchronisation de l'heure
    const uint32_t IntervalSyncHeure;

    TConnexionCellulaire(TRefDel<TDelClignotteTemp> delErreur,
                         uint32_t intervalSyncHeure);

    void begin();
    void loop();

    int SynchroniserHeure();
};

#endif /* TConnexionCellulaire_H */
