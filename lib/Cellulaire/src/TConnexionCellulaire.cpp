/**
 * @file TConnexionCellulaire.cpp
 * @brief Implémentation de TConnexionCellulaire
 */

#include "TConnexionCellulaire.h"
#include <Particle.h> // Cellular, Time
#include <stdio.h>    // sscanf
#include <time.h>     // mktime

//! Délai en ms entre les essai de connexion au réseau cellulaire
//! et de configuration du réseau cellulaire
#define DELAI_ESSAI 1000 // 1s

//! APN de la carte SIM externe.
//! Celui-ci est pour Public Mobile au Canada.
#define APN "sp.mb.com"

/**
 * @brief Initialise l'objet avec l'interval de synchronisation de l'heure
 * donnée
 *
 */
TConnexionCellulaire::TConnexionCellulaire(
    //! DEL pour signaler une erreur.
    TRefDel<TDelClignotteTemp> delErreur,
    //! Interval de synchronisation de l'heure
    uint32_t intervalSyncHeure) :
    FDelErreur(delErreur),
    FDerniereSyncHeure(0), IntervalSyncHeure(intervalSyncHeure) {
}

/**
 * @brief Initialise le modem cellulaire, le connecte au réseau
 * et effectue la synchronisation de l'heure
 *
 */
void TConnexionCellulaire::begin() {
    // Activer le modem cellulaire.
    Cellular.on();

    // Configurer la carte SIM externe.
    Cellular.setActiveSim(EXTERNAL_SIM);
    Cellular.setCredentials(APN);

    // Activer la synchronisation du fuseau horaire.
    while (Cellular.command("AT+CTZU=1") != RESP_OK) {
        FDelErreur.Del().FaireClignotter();
        Serial.println(
            "Impossible d'activer la synchronisation du fuseau horaire");
        delay(DELAI_ESSAI);
    }

    // Configurer le mode de texte brut pour les SMS.
    while (Cellular.command("AT+CMGF=1") != RESP_OK) {
        FDelErreur.Del().FaireClignotter();
        Serial.println(
            "Impossible de configurer le mode de texte brut pour les SMS");
        delay(DELAI_ESSAI);
    }

    // Se connecter au réseau cellulaire.
    Cellular.connect();
    while (!Cellular.ready()) {
        Serial.println("En attente d'être connecté au réseau cellulaire");
        delay(DELAI_ESSAI);
    }

    // Effectuer la première synchronisation de l'heure.
    while (SynchroniserHeure() != 0) {
        FDelErreur.Del().FaireClignotter();
        Serial.println("Impossible de synchroniser l'heure");
        delay(DELAI_ESSAI);
    }
}

/**
 * @brief Synchronise l'heure à l'interval donné
 * @see IntervalSyncHeure
 *
 */
void TConnexionCellulaire::loop() {
    if ((uint32_t)Time.now() > (FDerniereSyncHeure + IntervalSyncHeure)) {
        if (SynchroniserHeure() != 0) {
            FDelErreur.Del().FaireClignotter();
            Serial.println("Impossible de synchroniser l'heure");
        }
    }
}

//! Données passées à lireHeure représentant l'heure locale
struct THeureLocale {
    //! Heure
    time_t Heure;
    //! Fuseau horaire
    float FuseauHoraire;
};

/**
 * @brief Décode l'heure d'une réponse à une commande AT
 *
 * @return Un code d'erreur
 * @retval RESP_OK Aucune erreur n'est survenue
 */
static int cbLireHeure(
    //! Type de la réponse à la commande
    int type,
    //! Réponse à la commande
    const char *tampon,
    //! Taille en octets de `tampon`
    int tailleTampon,
    //! Heure locale à modifier
    THeureLocale *heureLocale) {
    int codeErreur = RESP_ERROR;

    if (type == TYPE_PLUS && tailleTampon == 33) {
        if (heureLocale != nullptr) {
            struct tm heure;
            const char *sTz =
                strptime(tampon, "\r\n+CCLK: \"%y/%m/%d,%H:%M:%S", &heure);
            if (sTz != nullptr) {
                int tz;
                if (sscanf(sTz, "%3d\"", &tz) == 1) {
                    heureLocale->Heure = mktime(&heure);
                    heureLocale->FuseauHoraire =
                        tz / 4.0; // Convertir le fuseau horaire de
                                  // tranches de 15min en heures.
                    codeErreur = RESP_OK;
                }
            }
        }
    }

    return codeErreur;
}

/**
 * @brief Effectue immédiatement la synchronisation de l'heure
 *
 * @return Un code d'erreur
 * @retval 0 La synchronisation a été réussie
 * @retval 1 Une erreur est survenue lors de la synchronisation
 */
int TConnexionCellulaire::SynchroniserHeure() {
    int codeErreur = 1;

    THeureLocale heureLocale;
    if (Cellular.command(cbLireHeure, &heureLocale, "AT+CCLK?") == RESP_OK) {
        codeErreur = 0;

        // Changer l'heure et le fuseau horaire.
        Time.setTime(heureLocale.Heure);
        Time.zone(heureLocale.FuseauHoraire);

        FDerniereSyncHeure = Time.now();
    }

    return codeErreur;
}
