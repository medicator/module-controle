/**
 * @file test.cpp
 * @brief Test de la connexion au réseau cellulaire et
 * de la synchronisation de l'heure
 */

#include <Arduino.h>
#include <TConnexionCellulaire.h>

// Ne pas attendre une connexion au réseau avant l'exécution de `void setup()`.
SYSTEM_THREAD(ENABLED);

// Ne pas se connecter automatiquement au réseau cellulaire.
SYSTEM_MODE(MANUAL);

//! DEL pour signaler une erreur.
TRefDel<TDelClignotteTemp> delErreur(D8);

//! Connexion au réseau cellulaire.
TConnexionCellulaire connCell(delErreur, 60);

void setup() {
    Serial.begin(9600);

    connCell.begin();
}

void loop() {
    connCell.loop();

    Serial.println(Time.timeStr());
    delay(1000);
}
