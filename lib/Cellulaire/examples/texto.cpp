/**
 * @file texto.cpp
 * @brief Test de la connexion cellulaire et de l'envoi de texto
 */

#include <Particle.h>
#include <TConnexionCellulaire.h>

// Ne pas attendre une connexion au réseau avant l'exécution de `void setup()`.
SYSTEM_THREAD(ENABLED);

// Ne pas se connecter automatiquement au réseau cellulaire.
SYSTEM_MODE(MANUAL);

//! DEL pour signaler une erreur.
TRefDel<TDelClignotteTemp> delErreur(D8);

//! Connexion au réseau cellulaire.
TConnexionCellulaire connCell(delErreur, 60);

//! Numéro de téléphone auquel envoyer le texto.
static const char NUM_TEL[] = "18199685918";
//! Message à envoyer par texto.
static const char MESSAGE[] = "Texto envoyer du distributeur de médicaments";

void setup() {
    Serial.begin(9600);

    // Initialiser et configurer la connexion cellulaire.
    connCell.begin();

    // Envoyer le texto.
    Cellular.sendSms(NUM_TEL, MESSAGE);
}

void loop() {
    Serial.print('.');
    delay(1000);
}
