/**
 * @file test.cpp
 * @brief Test de TAt25m02
 */

#include <Particle.h>
#include <TAt25m02.h>

// Ne pas attendre une connexion au réseau avant l'exécution de `void setup()`.
SYSTEM_THREAD(ENABLED);

// Ne pas se connecter automatiquement au réseau cellulaire.
SYSTEM_MODE(MANUAL);

//! EEPROM à tester.
TAt25m02 eeprom(SPI, A5);

//! Adresse à partir de laquelle le EEPROM doit être testé.
static const uint32_t ADRESSE_DEBUT_TEST = 0;
//! Taille de l'espace mémoire qui est testé.
//! Ceci ainsi que @code {ADRESSE_DEBUT_TEST} sont nécessaires, car la mémoire
//! du MCU n'est pas assez grande pour tester tout le EEPROM d'un seul coup.
static const uint32_t TAILLE_TEST = TAt25m02::TAILLE_MEMOIRE / 16;

//! Mémoire qui est comparé à celle du EEPROM pour s'assurer que toutes les
//! opérations effectués sur le EEPROM ont bien fonctionnées.
static uint8_t memoireEmulee[TAILLE_TEST];

//! Messages associés aux codes d'erreur passé à @code {gererErreur}.
static const char *MSGS_ERREUR[] = {
    "Aucune erreur", // 0
    "Le temps mort avant que le EEPROM soit prêt à recevoir une nouvelle "
    "commande",                                                    // 1
    "Le EEPROM n'a pas pu être configuré pour activer l'écriture", // 2
    "L'adresse ou le tampon dépasse la limite de la mémoire",      // 3
    "Le tampon est nul.",                                          // 4
    "Les données lues de la mémoire ne sont pas les mêmes qui y ont été "
    "écrites", // 5
};
//! Nombre de messages d'erreur.
static const int NB_MSGS_ERREUR = sizeof(MSGS_ERREUR) / sizeof(const char *);

//! Noms des opérations effectués sur le EEPROM par ce test.
static const char *NOMS_OPS[] = {
    "Écrire",          // 0
    "Écrire un octet", // 1
    "Lire",            // 2
};

//! Permet de faire clignotter la DEL d'état en rouge.
LEDStatus clignotteRouge(RGB_COLOR_RED, LED_PATTERN_BLINK, LED_SPEED_NORMAL,
                         LED_PRIORITY_IMPORTANT);

/**
 * @brief Lorsque le code d'erreur donné est différent de 0, le message d'erreur
 * donné est affiché ainsi que le message associé au code d'erreur
 *
 */
template <typename... Args>
void gererErreur(int codeErreur, const char *msg, Args... args) {
    if (codeErreur != 0) {
        // Faire clignotter la DEL d'état en rouge.
        clignotteRouge.setActive(true);

        // Bloquer l'exécution du programme.
        do {
            // Afficher le message d'erreur toutes les secondes.
            Serial.println();
            Serial.printlnf(msg, args...);
            if (codeErreur >= 0 && codeErreur < NB_MSGS_ERREUR) {
                Serial.printlnf("Code erreur #%i: %s", codeErreur,
                                MSGS_ERREUR[codeErreur]);
            } else {
                Serial.printlnf("Code d'erreur inconnu: %i", codeErreur);
            }
            delay(1000);
        } while (true);
    }
}

/**
 * @brief Enregistre la valeur 255 dans chaque octet de la mémoire émulée et du
 * EEPROM.
 */
void effacer() {
    // Effacer la mémoire émulée.
    for (uint32_t i = 0; i < TAILLE_TEST; i++) {
        memoireEmulee[i] = 255;
    }

    // Écrire la mémoire émulée dans la mémoire.
    int codeErreur =
        eeprom.Ecrire(ADRESSE_DEBUT_TEST, memoireEmulee, TAILLE_TEST);

    gererErreur(codeErreur, "Erreur lors de l'effacement de la mémoire");
}

void setup() {
    // Désactiver la DEL.
    clignotteRouge.setActive(false);

    // Initialiser la communication sur le port sériel.
    Serial.begin(9600);
    delay(2000);

    // Initialiser le EEPROM.
    int codeErreur;
    do {
        codeErreur = eeprom.begin();
        if (codeErreur != 0) {
            Serial.printlnf("Configuration du EEPROM: %i", codeErreur);
        }
    } while (codeErreur != 0);
    Serial.println("EEPROM configuré avec succès");

    effacer();
    Serial.println("Mémoire effacée");
}

/**
 * @brief Écrit une liste d'octets dans le EEPROM et dans la mémoire émulée
 */
int ecrire(
    //! Adresse à partir de laquelle écrire dans le EEPROM
    uint32_t adresse,
    //! Tampon à écrire en mémoire
    const uint8_t *tampon,
    //! Taille du tampon
    uint32_t tailleTampon) {
    int codeErreur =
        eeprom.Ecrire(adresse + ADRESSE_DEBUT_TEST, tampon, tailleTampon);
    if (codeErreur == 0) {
        for (uint32_t i = 0; i < tailleTampon; i++) {
            memoireEmulee[adresse + i] = tampon[i];
        }
    }

    gererErreur(
        codeErreur,
        "Impossible d'écrire dans le EEPROM. adresse: %i, taille du tampon: %i",
        adresse, tailleTampon);

    return codeErreur;
}

/**
 * @brief Écrit un octet dans le EEPROM et dans la mémoire émulée
 */
void ecrireOctet(
    //! Adresse à partir à laquelle écrire l'octet
    uint32_t adresse,
    //! Octet à écrire en mémoire
    uint8_t octet) {
    int codeErreur = eeprom.EcrireOctet(adresse + ADRESSE_DEBUT_TEST, octet);
    if (codeErreur == 0) {
        memoireEmulee[adresse] = octet;
    }

    gererErreur(codeErreur,
                "Impossible d'écrire un seul octet dans le EEPROM. adresse: "
                "%li, octet: %i",
                adresse, octet);
}

/**
 * @brief Lit une liste d'octets du EEPROM et les compare à la mémoire émulée
 * pour s'assurer qu'ils sont bons.
 */
void lire(
    //! Adresse à partir de laquelle lire les données du EEPROM
    uint32_t adresse,
    //! Tampon dans lequel écrire les données lues.
    uint8_t *tampon,
    //! Taille du tampon
    uint32_t tailleTampon) {
    int codeErreur =
        eeprom.Lire(adresse + ADRESSE_DEBUT_TEST, tampon, tailleTampon);
    if (codeErreur == 0) {
        // S'assurer que les données lues sont les mêmes qui sont dans la
        // mémoire émulée.
        if (memcmp(&memoireEmulee[adresse], tampon, tailleTampon) != 0) {
            codeErreur = 5;
        }
    }

    gererErreur(codeErreur,
                "Erreur de lecture. adresse: %li, taille du tampon: %li",
                adresse, tailleTampon);
}

void loop() {
    //! Tampon utilisé pour les différentes opérations.
    static uint8_t tamponOperations[TAILLE_TEST];

    // Générer une adresse aléatoire et une taille de tampon.
    const uint32_t adresse = random() % TAILLE_TEST;
    const uint32_t tailleTampon = random() % (TAILLE_TEST - adresse + 1);

    // Exécuter une opération aléatoire.
    uint8_t op = random() % 3;
    Serial.printlnf(
        "opération: %s, adresse: %li, taille tampon: %li, fin tampon: %li",
        NOMS_OPS[op], adresse, tailleTampon, (adresse + tailleTampon));
    switch (op) {
    case 0:
        // Placer des données aléatoires dans le tampon.
        for (uint32_t i = 0; i < tailleTampon; i++) {
            tamponOperations[i] = random();
        }
        ecrire(adresse, tamponOperations, tailleTampon);
        break;
    case 1:
        ecrireOctet(adresse, random());
        break;
    case 2:
        lire(adresse, tamponOperations, tailleTampon);
        break;
    }
}
