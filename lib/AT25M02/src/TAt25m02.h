/**
 * @file TAt25m02.h
 * @brief Déclaration de TAt25m02 et TAt25m02::TRegStatut
 */

#ifndef TAt25m02_H
#define TAt25m02_H

#include <SPI.h>
#include <functional>
#include <stdint.h>

//! Permet d'utiliser un module de mémoire EEPROM AT25M02 et d'exécuter toutes
//! les commandes qu'il supporte.
class TAt25m02 {
private:
    //! Niveau logique de la broche EEPROM pour selectionner le EEPROM.
    static const uint8_t EEPROM_SELECTIONNE = LOW;
    //! Niveau logique de la broche EEPROM pour déselectionner le EEPROM.
    static const uint8_t EEPROM_DESELECTIONNE = HIGH;

    //! Code de l'opération pour lire le registre de statut.
    static const uint8_t OPCODE_RDSR = 0x05;
    //! Code de l'opération pour lire si le EEPROM est dans un cycle d'écriture.
    static const uint8_t OPCODE_LPWP = 0x08;
    //! Code de l'opération pour activer le WEL.
    static const uint8_t OPCODE_WREN = 0x06;
    //! Code de l'opération pour désactive le WEL.
    static const uint8_t OPCODE_WRDI = 0x04;
    //! Code de l'opération pour écrire dans le registre de statut.
    static const uint8_t OPCODE_WRSR = 0x01;
    //! Code de l'opération pour lire des octets de la mémoire.
    static const uint8_t OPCODE_READ = 0x03;
    //! Code de l'opération pour écrire un seul octet dans la mémoire.
    static const uint8_t OPCODE_WRITE_BYTE = 0x02;
    //! Code de l'opération pour écrire une page dans la mémoire.
    static const uint8_t OPCODE_WRITE_PAGE = 0x07;

    void ExecuterOperation(uint8_t opcode, std::function<void(void)> fn);
    int TransfertSynchroneRapide(const uint8_t *tx, uint8_t *rx,
                                 uint32_t taille);

public:
    //! Registre de statut du EEPROM.
    union TRegStatut {
        //! Le EEPROM est prêt.
        static const uint8_t RDY = 0;
        //! Le EEPROM est occupé.
        static const uint8_t BSY = 1;

        //! L'écriture est désactivée.
        static const uint8_t WEL_ECRITURE_DESACTIVEE = 0;
        //! L'écriture est activée.
        static const uint8_t WEL_ECRITURE_ACTIVEE = 1;

        //! Aucune protection, tous les blocs peuvent être écrits.
        static const uint8_t BP_AUCUNE_PROTECTION = 0;
        //! Protection du premier quart de la mémoire.
        static const uint8_t BP_PROTECTION_QUART = 1;
        //! Protection de la moitié de la mémoire.
        static const uint8_t BP_PROTECTION_MOITIE = 2;
        //! Protection de toute la mémoire.
        static const uint8_t BP_PROTECTION_COMPLETE = 3;

        //! Un cycle d'écriture est en cours.
        static const uint8_t RFU_ECRITURE_TERMINEE = 0;
        //! Un cycle d'écriture n'est pas en cours.
        static const uint8_t RFU_ECRITURE_EN_COURS = 7;

        //! Protection d'écriture activée.
        static const uint8_t WPEN_PROTECTION_ACTIVEE = 0;
        //! Protection d'écriture désactivée. On peut écrire dans le EEPROM
        //! à condition que WEL soit activé et que la broche WP soit à un
        //! niveau logique haut.
        static const uint8_t WPEN_PROTECTION_DESACTIVEE = 1;

        struct {
            //! Si le EEPROM est occupé ou prêt, RO.
            //! Valeurs:
            //! * RDY
            //! * BSY
            uint8_t RDY_BSY : 1; // LSB

            //! Activation de l'écriture, R/W. Ceci est désactivé au démarrage
            //! et est désactivé de nouveau après chaque écriture. Valeurs:
            //! * WEL_ECRITURE_DESACTIVEE
            //! * WEL_ECRITURE_ACTIVEE
            uint8_t WEL : 1;

            //! Configuration de la protection de la mémoire, R/W.
            //! Valeurs:
            //! * BP_AUCUNE_PROTECTION
            //! * BP_PROTECTION_QUART
            //! * BP_PROTECTION_MOITIE
            //! * BP_PROTECTION_COMPLETE
            uint8_t BP : 2;

            //! Réserver, RO.
            //! Valeurs:
            //! * RFU_ECRITURE_TERMINEE
            //! * RFU_ECRITURE_EN_COURS
            uint8_t RFU : 3;

            //! Protection d'écriture, R/W. Ceci est à 0 lorsque le AT25M02
            //! démarre. Valeurs:
            //! * WPEN_PROTECTION_ACTIVEE
            //! * WPEN_PROTECTION_DESACTIVEE
            uint8_t WPEN : 1; // MSB
        };
        //! Valeur du registre.
        uint8_t Valeur;

        TRegStatut(uint8_t statut = 0);
        TRegStatut(uint8_t wpen, uint8_t bp);
    };

    //! Port SPI pour communiquer avec le EEPROM.
    SPIClass &PortSPI;
    //! Broche pour selectionner le EEPROM sur le port SPI.
    const int CS;
    //! Temps mort en ms pour communiquer avec le EEPROM.
    uint32_t TempsMort;

    //! Taille de la mémoire en octets.
    static const uint32_t TAILLE_MEMOIRE = 262144; // 256kio, 2Mbit
    //! Taille d'une page en octets.
    static const uint16_t TAILLE_PAGE = 256; // 256o

    TAt25m02(SPIClass &portSPI, int cs, uint32_t tempsMort = 1000);

    int begin(int frequence = 4000000);

    TRegStatut LireStatut();

    int AttendrePret(TRegStatut *statut = nullptr);
    int PreparerEcrire(TRegStatut *statut = nullptr);

    int Lire(uint32_t adresse, uint8_t *tampon, uint32_t tailleTampon);
    int EcrireOctet(uint32_t adresse, uint8_t donnee);
    int EcrirePage(uint32_t adresse, const uint8_t *tampon,
                   uint16_t tailleTampon);
    int Ecrire(uint32_t adresse, const uint8_t *tampon, uint32_t tailleTampon);
};

// S'assurer que la taille du type de registre de statut est de 1 octet.
static_assert(
    sizeof(TAt25m02::TRegStatut) == 1,
    "TAt25m02::TRegStatut doit avoir une taille d'exactement 1 octet.");

#endif // TAt25m02_H
