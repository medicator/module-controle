/**
 * @file TAt25m02.cpp
 * @brief Implémentation de TAt25m02 et TAt25m02::TRegStatut
 */

#include "TAt25m02.h"

/**
 * @brief Initialise un registre de statut avec la valeur donnée
 */
TAt25m02::TRegStatut::TRegStatut(
    //! Valeur du registre de statut
    uint8_t statut) {
    Valeur = statut;
}

/**
 * @brief Initialise un registre de statut à 0, puis change les
 * propriétés WPEN et BP pour les valeurs données.
 * @note Ceci est utile pour effectuer l'opération WRSR
 */
TAt25m02::TRegStatut::TRegStatut(
    //! Valeur de WPEN
    uint8_t wpen,
    //! Valeur de BP
    uint8_t bp) {
    Valeur = 0;
    WPEN = wpen;
    bp = BP;
}

/**
 * @brief Initialise un nouvel objet de la classe sans le configurer
 * @note Ne pas oublier d'appeler @code {begin} pour configurer le port SPI
 * et la broche de CS.
 */
TAt25m02::TAt25m02(
    //! Port SPI pour communiquer avec le EEPROM.
    SPIClass &portSPI,
    //! Broche pour selectionner le EEPROM pour la communication.
    int cs,
    //! Temps mort en ms pour communiquer avec le EEPROM.
    //! La valeur par défaut est de 1s.
    uint32_t tempsMort) :
    PortSPI(portSPI),
    CS(cs), TempsMort(tempsMort) {
}

/**
 * @brief Établie une communication avec le EEPROM, envoi le opcode donné, puis
 * exécute la fonction donnée, puis met fin à la communication.
 */
inline void TAt25m02::ExecuterOperation(
    //! Code de l'opération à exécuter au début de la communication
    uint8_t opcode,
    //! Fonction à exécuter après l'envoi du opcode, mais avant la fin de la
    //! communication
    std::function<void(void)> fn) {
    // Initialiser la communication.
    digitalWrite(CS, EEPROM_SELECTIONNE);

    // Transférer le opcode.
    PortSPI.transfer(opcode);

    // Exécuter la fonction donnée.
    fn();

    // Mettre fin à la communication.
    digitalWrite(CS, EEPROM_DESELECTIONNE);
}

//! Mutex pour empêcher l'accès en parallèle à @code {semaphores}.
static Mutex mutexTransfertRapide;
//! List de bits utilisés pour savoir si un transfert synchrone est terminé.
static volatile uint32_t semaphores;

/**
 * @brief Fonction à passer à SPI::transfer(void*, void*, uint32_t,
 * void*(void)). Elle permet de désactiver le premier sémaphore activé de la
 * variables @code {semaphores}.
 *
 */
void cbTransfertSynchroneRapide() {
    static volatile uint8_t idxSemaphore =
        0; // Index du sémaphore qu'il faut désactiver.

    // Désactiver le sémaphore à l'index actuel.
    semaphores &= ~(1 << idxSemaphore);

    // Passer au prochain sémaphore lors du prochain appel.
    idxSemaphore = (idxSemaphore + 1) % 32;
}

/**
 * @brief Utilise la méthode SPI::transfer(void*, void*, uint32_t, void*(void))
 * pour effectuer un transfert rapide de données puis attend que le transfert
 * soit terminé avant de retourner.
 * @note Il est important d'activer le CS du EEPROM avant d'appeler cette
 * méthode. Cette méthode ne désactive pas non plus le CS lorsqu'elle a terminé.
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Trop de transferts sont déjà en attente
 */
int TAt25m02::TransfertSynchroneRapide(
    //! Données à transférer ou nul
    const uint8_t *tx,
    //! Tampon ou écrire les données ou nul
    uint8_t *rx,
    //! Quantité de données à lire/écrire sur le port SPI.
    uint32_t taille) {
    static volatile uint8_t idxSemaphore = 0;

    int codeErreur = 1;

    if (taille > 0) {
        // Tenter d'effectuer le transfert.
        mutexTransfertRapide.lock();
        uint32_t masque = 1 << idxSemaphore;
        if ((semaphores & masque) == 0) {
            semaphores |= masque;
            PortSPI.transfer((void *)tx, (void *)rx, taille,
                             cbTransfertSynchroneRapide);
            codeErreur = 0;
        }
        idxSemaphore = (idxSemaphore + 1) % 32;
        mutexTransfertRapide.unlock();

        if (codeErreur == 0) {
            // Attendre que le transfert soit terminé.
            while ((semaphores & masque) != 0) {
            }
        }
    } else {
        codeErreur = 0;
    }

    return codeErreur;
}

/**
 * @brief Configure:
 *  * le port SPI en mode 0 et avec le MSB en premier
 *  * La broche CS en sortie
 *  * Le EEPROM pour qu'on puisse écrire n'importe où
 *    dedans
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur n'est survenue
 * @retval 1 Le EEPROM n'a pas pu être configuré. Il se peut que les broches
 * WP et HOLD du EEPROM ne soient pas au bon niveau logique ou qu'une erreur
 * de communication soit survenue.
 */
int TAt25m02::begin(
    //! Fréquence de communication avec le EEPROM.
    //! 4MHz par défaut.
    int frequence) {
    int codeErreur = 0;

    // Configurer le port SPI.
    PortSPI.setDataMode(SPI_MODE0);
    PortSPI.setBitOrder(MSBFIRST);
    PortSPI.setClockSpeed(frequence);
    PortSPI.begin(SPI_MODE_MASTER, CS);

    // Configurer la broche CS et déselectionner le EEPROM.
    pinMode(CS, OUTPUT);
    digitalWrite(CS, EEPROM_DESELECTIONNE);

    // Configurer le EEPROM pour qu'on puisse écrire n'importe où dedans.
    TRegStatut desactiverProtections(TRegStatut::WPEN_PROTECTION_DESACTIVEE,
                                     TRegStatut::BP_AUCUNE_PROTECTION);
    ExecuterOperation(OPCODE_WRSR, [desactiverProtections, this]() {
        PortSPI.transfer(desactiverProtections.Valeur);
    });

    // Vérifier la configuration du EEPROM.
    TRegStatut statut = LireStatut();
    if (statut.BP != TRegStatut::BP_AUCUNE_PROTECTION ||
        statut.WPEN != TRegStatut::WPEN_PROTECTION_DESACTIVEE) {
        codeErreur = -1;
    }

    return codeErreur;
}

/**
 * @brief Lit le statut du EEPROM
 *
 * @return Le statut du EEPROM
 */
TAt25m02::TRegStatut TAt25m02::LireStatut() {
    uint8_t resultat;
    ExecuterOperation(OPCODE_RDSR,
                      [&resultat, this]() { resultat = PortSPI.transfer(0); });

    return TRegStatut(resultat);
}

/**
 * @brief Attend que le EEPROM soit prêt ou que le temps mort soit atteint.
 *
 * @return Un code d'erreur
 * @retval 0 le EEPROM est prêt à recevoir une nouvelle commande.
 * @retval 1 le temps mort a été atteint.
 */
int TAt25m02::AttendrePret(
    //! Statut du EEPROM
    //! Si cette valeur n'est pas nulle, le registre de statut sera placé
    //! dans l'espace pointé à la fin de la fonction peut importe que le
    //! temps mort ait été atteint ou non.
    TRegStatut *statut) {
    int codeErreur = 0;
    TRegStatut reg;

    // Lire le statut du EEPROM
    const uint32_t debut = millis();
    ExecuterOperation(OPCODE_RDSR, [&reg, &codeErreur, debut, this]() {
        do {
            reg.Valeur = PortSPI.transfer(0);

            // Si le temps mort est écoulé.
            if ((millis() - debut) > TempsMort) {
                codeErreur = 1;
            }
        } while (reg.RDY_BSY != TRegStatut::RDY && codeErreur == 0);
    });

    // Assigner le registre de statut au registre de statut reçu en
    // paramètre.
    if (statut != nullptr) {
        *statut = reg;
    }

    // Retourner un code d'erreur en fonction de si le temps mort a été
    // atteint ou non.
    return codeErreur;
}

/**
 * @brief Prépare le EEPROM à ce qu'on écrive dedans
 * Il active le bit WEL, puis attend que le EEPROM soit prêt à recevoir une
 * commande.
 *
 * @return Un code d'erreur
 * @retval 0 le EEPROM est prêt à ce qu'on écrive dedans.
 * @retval 1 le EEPROM a atteint le temps mort avant d'être prêt à recevoir
 * une commande.
 * @retval 2 le EEPROM n'a pas pu être configuré pour activer l'écriture.
 */
int TAt25m02::PreparerEcrire(
    //! Statut du EEPROM
    //! Si cette valeur n'est pas nulle, le registre de statut sera placé
    //! dans l'espace pointé à la fin de la fonction peut importe que le
    //! temps mort ait été atteint ou non.
    TRegStatut *statut) {
    TRegStatut reg;
    int codeErreur = AttendrePret(&reg);

    if (codeErreur == 0 && reg.WEL == TRegStatut::WEL_ECRITURE_DESACTIVEE) {
        ExecuterOperation(OPCODE_WREN, []() {});

        // S'assurer que l'écriture soit activée et attendre que le EEPROM soit
        // prêt à recevoir une nouvelle commande.
        codeErreur = AttendrePret(&reg);
        if (reg.WEL == TRegStatut::WEL_ECRITURE_DESACTIVEE) {
            codeErreur = 2;
        }
    }

    if (statut != nullptr) {
        *statut = reg;
    }

    return codeErreur;
}

/**
 * @brief Lit le nombre d'octets donnés à l'adresse donnée du EEPROM
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur n'est survenue
 * @retval 1 Un temps mort est survenu
 * @retval 2 Le paramètre @code {tampon} est nul
 * @retval 5 trop de communications sont en cours.
 */
int TAt25m02::Lire(
    //! Adresse à partir de laquelle lire les données dans le EEPROM.
    uint32_t adresse,
    //! Tampon dans lequel placer les données lues.
    uint8_t *tampon,
    //! Taille du tampon.
    uint32_t tailleTampon) {
    int codeErreur = 2;

    if (tampon != NULL) {
        codeErreur = AttendrePret();

        if (codeErreur == 0) {
            ExecuterOperation(OPCODE_READ, [&tampon, tailleTampon, adresse,
                                            &codeErreur, this]() {
                // Transférer l'adresse.
                PortSPI.transfer(adresse >> 16);
                PortSPI.transfer(adresse >> 8);
                PortSPI.transfer(adresse);

                // Lire les données.
                if (TransfertSynchroneRapide(nullptr, tampon, tailleTampon) !=
                    0) {
                    codeErreur = 5;
                }
            });
        }
    }

    return codeErreur;
}

/**
 * @brief Écrit un seul octet à l'adresse donnée dans le EEPROM
 *
 * @return Un code d'erreur
 * @retval 0 le EEPROM est prêt à ce qu'on écrive dedans.
 * @retval 1 le EEPROM a atteint le temps mort avant d'être prêt à recevoir
 * une commande.
 * @retval 2 le EEPROM n'a pas pu être configuré pour activer l'écriture.
 * @retval 3 l'adresse donnée est invalide
 */
int TAt25m02::EcrireOctet(
    //! Adresse ou enregistrer l'octet donné
    uint32_t adresse,
    //! Octet à enregistrer.
    uint8_t donnee) {
    int codeErreur = 3;

    if (adresse < TAILLE_MEMOIRE) {
        codeErreur = PreparerEcrire();

        if (codeErreur == 0) {
            ExecuterOperation(OPCODE_WRITE_BYTE, [adresse, donnee, this]() {
                // Transférer l'adresse.
                PortSPI.transfer(adresse >> 16);
                PortSPI.transfer(adresse >> 8);
                PortSPI.transfer(adresse);

                // Transférer l'octet.
                PortSPI.transfer(donnee);
            });
        }
    }

    return codeErreur;
}

/**
 * @brief Écrit les octets donnés à l'adresse données du EEPROM
 * @note Si les octets donnés sont dans plusieurs pages,
 *
 * @return Un code d'erreur
 * @retval 0 le EEPROM est prêt à ce qu'on écrive dedans.
 * @retval 1 le EEPROM a atteint le temps mort avant d'être prêt à recevoir
 * une commande.
 * @retval 2 le EEPROM n'a pas pu être configuré pour activer l'écriture.
 * @retval 3 l'adresse donnée est invalide ou le tampon dépasse la limite de la
 * mémoire
 * @retval 4 le paramètre @code {tampon} est nul.
 * @retval 5 trop de communications sont en cours.
 */
int TAt25m02::EcrirePage(
    //! Adresse à partir de laquelle lire les données dans le EEPROM.
    uint32_t adresse,
    //! Tampon contenant les données à écrire.
    const uint8_t *tampon,
    //! Taille du tampon.
    uint16_t tailleTampon) {
    int codeErreur = 4;

    if (tampon != nullptr) {
        if (adresse < TAILLE_MEMOIRE && tailleTampon <= TAILLE_PAGE &&
            (adresse + tailleTampon) < TAILLE_MEMOIRE) {
            codeErreur = PreparerEcrire();
        } else {
            codeErreur = 3;
        }
    }

    if (codeErreur == 0) {
        ExecuterOperation(OPCODE_WRITE_PAGE, [adresse, tampon, tailleTampon,
                                              &codeErreur, this]() {
            // Transférer l'adresse.
            PortSPI.transfer(adresse >> 16);
            PortSPI.transfer(adresse >> 8);
            PortSPI.transfer(adresse);

            // Transférer les données.
            if (TransfertSynchroneRapide(tampon, nullptr, tailleTampon) != 0) {
                codeErreur = 5;
            }
        });
    }

    return codeErreur;
}

/**
 * @brief Écrit les octets donnés à l'adresse données du EEPROM
 * @note Cette méthode va séparer le tampon donné en différentes page pour les
 * écrire le plus efficacement possible dans le EEPROM
 *
 * @return Un code d'erreur
 * @retval 0 le EEPROM est prêt à ce qu'on écrive dedans.
 * @retval 1 le EEPROM a atteint le temps mort avant d'être prêt à recevoir
 * une commande.
 * @retval 2 le EEPROM n'a pas pu être configuré pour activer l'écriture.
 * @retval 3 l'adresse donnée est invalide ou le tampon dépasse la limite de la
 * mémoire
 * @retval 4 le paramètre @code {tampon} est nul.
 * @retval 5 trop de communications sont en cours.
 */
int TAt25m02::Ecrire(
    //! Adresse à partir de laquelle lire les données dans le EEPROM.
    uint32_t adresse,
    //! Tampon contenant les données à écrire.
    const uint8_t *tampon,
    //! Taille du tampon.
    uint32_t tailleTampon) {
    int codeErreur = 4;

    if (tampon != nullptr) {
        // Valider l'adresse et la taille du tampon.
        codeErreur = (adresse < TAILLE_MEMOIRE &&
                      tailleTampon <= (TAILLE_MEMOIRE - adresse))
                         ? 0
                         : 3;
    }

    if (codeErreur == 0) {
        // Tant que la fin du tampon n'a pas été atteinte
        uint32_t indexTampon = 0;
        while (indexTampon < tailleTampon && codeErreur == 0) {
            // Calculer l'adresse de la page et le nombre d'octets à écrire.
            uint32_t adressePage = (adresse + indexTampon) -
                                   ((adresse + indexTampon) % TAILLE_PAGE);
            uint16_t donneesPage =
                TAILLE_PAGE - ((adresse + indexTampon) % TAILLE_PAGE);

            // Limiter le nombre de données à envoyer au nombre de données
            // restantes.
            uint16_t donneesRestantes = tailleTampon - indexTampon;
            if (donneesPage > donneesRestantes) {
                donneesPage = donneesRestantes;
            }

            codeErreur = EcrirePage(max(adressePage, adresse),
                                    &tampon[indexTampon], donneesPage);
            indexTampon += donneesPage;
        }
    }

    return codeErreur;
}
