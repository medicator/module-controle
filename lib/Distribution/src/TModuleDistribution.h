/**
 * @file TModuleDistribution.h
 * @brief Déclaration de TModuleDistribution
 */

#ifndef TModuleDistribution_H
#define TModuleDistribution_H

#include "TContact.h"
#include "THoraireDistribution.h"
#include "TNotification.h"
#include <TBuzzer.h>
#include <TDelClignotteTemp.h>
#include <TGestionnaireEEPROM.h>

//! Nombre de jours dans une semaine.
static const size_t NB_JOUR_SEMAINE = 7;
//! Noms des jours de la semaine (en commençant par dimanche).
extern const char *NOMS_JOURS_SEMAINE[NB_JOUR_SEMAINE];

//! Taille maximale du nom d'un médicament
#define TAILLE_NOM_MEDIC_MAX 32

//! Gère le niveau de médicament, la distribution de médicaments
//! et l'état d'un module de distribution.
class TModuleDistribution {
private:
    //! DEL à faire clignotter pour indiquer la disponibilité de médicaments.
    TRefDel<TDel> FDelMedicDispo;
    //! DEL à faire clignotter en cas d'erreur.
    TRefDel<TDelClignotteTemp> FDelErreur;

    //! État non volatile de cet objet.
    struct {
        //! Nombre de médicaments dans le réservoir à partir duquel
        //! il est considéré comme critique.
        uint16_t NbMedicCritique;

        //! Nombre de médicaments dans le réservoir.
        uint16_t MedicReservoir;
        //! Nombre de médicaments à distribuer.
        uint8_t MedicADistribuer;

        //! Nom du médicament contenu dans le distributeur.
        char NomMedic[TAILLE_NOM_MEDIC_MAX + 1];

        //! Horaire de distribution.
        THoraireDistribution Horaire;

        //! Si le capteur de niveau critique signale que le niveau
        //! du réservoir est critique.
        bool NiveauEstCritique;

        //! Heure de la dernière distribution effectuée.
        time_t DerniereDistribution;

        //! Nombre de sec de retard avant que la notification de retard soit
        //! envoyée.
        uint32_t DelaiRetard;

        //! Si l'utilisateur doit s'authentifier.
        bool AttenteAuth;
    } FEtat;

    //! Allocation d'un espace dans le EEPROM pour y enregistrer l'état non
    //! volatile de cet objet.
    TAllocationEEPROM FAllocation;
    //! Si l'état non volatile doit être enregistré.
    bool FEnregistrement;

    //! Si le nombre de médicaments à distribuer doit être envoyé.
    bool FEnvoiNbMedic;

    //! Notification de médicaments à prendre.
    TNotification<const char *, int> FNotifMedicAPrendre;
    //! Notification signifiant qu'il a manqué de médicaments pour une
    //! distribution.
    TNotification<const char *, int, int> FNotifManqueMedic;
    //! Notification de niveau critique.
    TNotification<const char *> FNotifNiveauCritique;
    //! Si la notification a déjà été envoyée.
    TNotification<const char *, int, int> FNotifNbCritique;
    //! Notification du début du retard sur la prise de médicament.
    TNotification<const char *, const char *, int, int, int> FNotifDebutRetard;
    //! Notification de la fin du retard sur la prise de médicament.
    TNotification<const char *, int, int, int, int> FNotifFinRetard;
    //! Notification de prise de médicaments ratée.
    TNotification<const char *, const char *, int, int, int> FNotifPriseRatee;

    template <typename... Props>
    void RestaurerNotification(TNotification<Props...> &notif, const char *msg,
                               TGestionnaireEEPROM &gestionnaireEEPROM);

public:
    //! Liste de contacts.
    TContacts Contacts;

    /**
     * @brief Fonction à appeler pour demander le changement du nombre de
     * médicaments au module de distribution
     *
     * @return Si l'envoi a été réussi
     * @retval true L'envoi a été réussi
     * @retval false Une erreur est survenue
     */
    bool (*EnvoyerNbMedic)(
        //! Nombre de médicaments à distribuer
        uint8_t nbMedic);
    /**
     * @brief Fonction à appeler pour déverrouiller la recharge
     *
     * @return Un code d'erreur
     * @see TCommunication::DeverrouillerRecharge
     */
    int (*DeverrouillerRecharge)(
        //! Nombre de secondes de déverrouillage
        uint8_t sec);

    TModuleDistribution(TRefDel<TDel> delMedicDispo,
                        TRefDel<TDelClignotteTemp> delErreur);

    void begin(TGestionnaireEEPROM &gestionnaireEEPROM);
    void loop();

    bool DistribuerMedic();
    void NiveauMedicCritique(bool estCritique);

    bool Authentifier();
    int Deverrouiller(uint8_t sec);

    THoraireDistribution &Horaire();
    const THoraireDistribution &Horaire() const;

    const char *NomMedic();
    int NomMedic(const char *nom);

    uint8_t MedicADistribuer() const;
    uint16_t MedicReservoir() const;
    void MedicReservoir(uint16_t medicReservoir);

    uint16_t NbMedicCritique() const;
    void NbMedicCritique(uint16_t nbCritique);

    uint32_t DelaiRetard() const;
    void DelaiRetard(uint32_t delaiRetard);
};

#endif /* TModuleDistribution_H */
