/**
 * @file Menu.h
 * @brief Déclaration de TItemMenu, AfficherMenu, GererMenu et des fonctions des
 * items du menu
 */

#ifndef Menu_H
#define Menu_H

#include <TCapteurEmpreintes.h>   // TCapteurEmpreintes
#include <THoraireDistribution.h> // THoraireDistribution
#include <TModuleDistribution.h>  // TModuleDistribution, TContacts
#include <functional>             // std::function

//! Représente un item du menu.
struct TItemMenu {
    //! Nom de l'item.
    const char *Nom;
    //! Fonction à appeler lorsque l'item est sélectionné.
    std::function<void(void)> Selectionner;

    TItemMenu(const char *nom, std::function<void(void)> selectionner);
};

int AfficherMenu(const TItemMenu *items, size_t nbItems,
                 const char *messageErreur = nullptr);
int GererMenu(const TItemMenu *items, size_t nbItems);

//==============================================================================
// Fonctions des items du menu
//==============================================================================

void MenuAfficherNbMedic(TModuleDistribution &module);
void MenuChangerNbMedic(TModuleDistribution &module);

void MenuAfficherNbMedicCritique(TModuleDistribution &module);
void MenuChangerNbMedicCritique(TModuleDistribution &module);

void MenuAfficherDelaiRetard(TModuleDistribution &module);
void MenuChangerDelaiRetard(TModuleDistribution &module);

void MenuAfficherNomMedic(TModuleDistribution &module);
void MenuChangerNomMedic(TModuleDistribution &module);

void MenuDeverrouiller(TModuleDistribution &module);

void MenuEnregistrerEmpreinte(TCapteurEmpreintes &capteur);
void MenuSupprimerEmpreinte(TCapteurEmpreintes &capteur);

void MenuAfficherContacts(const TContacts &contacts);
void MenuAjouterContact(TContacts &contacts);
void MenuSupprimerContact(TContacts &contacts);

void MenuAfficherHoraireDistribution(const THoraireDistribution &horaire);
void MenuModifierHoraireDistribution(THoraireDistribution &horaire);

void MenuAfficherHeureSysteme();
void MenuChangerHeureSysteme();

#endif /* Menu_H */
