/**
 * @file TNotification.h
 * @brief Déclaration de TNotification
 */

#ifndef TNotification_H
#define TNotification_H

#include "TContact.h"
#include <Arduino.h> // String
#include <TGestionnaireEEPROM.h>
#include <tuple> // std::tuple

//! Représente une notification à envoyer aux contacts par texto.
template <typename... Props> class TNotification {
private:
    //! État non volatile de cet objet.
    struct {
        //! Si la notification est activée et qu'elle doit donc être envoyée.
        bool Activee;
        //! Propriétés du messsage de la notification.
        std::tuple<Props...> PropsMsg;
    } FEtat;

    //! Allocation dans le EEPROM pour enregistrer
    //! l'état non volatile de cet objet.
    TAllocationEEPROM FAllocation;

    //! Message de la notification.
    String FMessage;
    //! Format du message.
    const char *FFormat;

    /**
     * @brief Compose le message à envoyer aux contacts pour les propriétés
     * données pour la notification
     *
     * @return Le message de la notification
     */
    String Message(Props... props) {
        return String::format(FFormat, props...);
    }

    /**
     * @brief Restaure le message à l'aide de FEtat.PropsMsg
     *
     * @tparam Idx Les indexs des éléments du tuple FEtat.PropsMsg
     * @return Le message restauré
     */
    template <size_t... Idx>
    String RestaurerMessage(std::index_sequence<Idx...>) {
        return Message(std::get<Idx>(FEtat.PropsMsg)...);
    }

public:
    //! Raison de la notification.
    const TRaisonContact Raison;

    /**
     * @brief Initialise la notification avec le format de message
     * et la raison donnés
     *
     */
    TNotification(
        //! Format du message de la notification.
        const char *format,
        //! Raison de cette notification
        TRaisonContact raison) :
        FFormat(format),
        Raison(raison) {
        FEtat.Activee = false;
    }

    /**
     * @brief Restaurer l'état de la notification du EEPROM
     * @note Si cette méthode retourne un résultat différent de 0, l'état
     * précédent de l'objet est conservé.
     *
     * @return Un code d'erreur
     * @retval 0 Aucune erreur
     * @retval * Voir TAllocationEEPROM::Lire
     */
    int Restaurer(
        //! Gestionnaire du EEPROM
        TGestionnaireEEPROM &gestionnaireEEPROM) {
        gestionnaireEEPROM.Allouer(sizeof(FEtat), FAllocation);
        int codeErreur = FAllocation.Lire(FEtat);
        if (codeErreur == 0 && FEtat.Activee) {
            FMessage =
                RestaurerMessage(std::make_index_sequence<sizeof...(Props)>());
        }
        return codeErreur;
    }

    /**
     * @brief Activer la notification afin qu'elle soit envoyée à tous les
     * contacts concernés
     *
     */
    void Activer(
        //! Contact
        TContacts &contacts,
        //! Propriétés de la notification
        Props... props) {
        FEtat.Activee = true;

        // Retirer la raison de contact.
        TContact *mContacts = contacts.Contacts();
        for (size_t i = 0; i < NB_CONTACTS; i++) {
            TContact &contact = mContacts[i];
            if (contact.EstActif()) {
                contact.RaisonsEnvoyees.Retirer(Raison);
            }
        }

        // Composer le message de la notification.
        FEtat.PropsMsg = std::tuple<Props...>(props...);
        FMessage = Message(props...);

        contacts.Enregistrer();
        FAllocation.Ecrire(FEtat);
    }

    /**
     * @brief Désactive la notification de manière à ce qu'elle ne soit plus
     * envoyée
     *
     */
    void Desactiver() {
        if (FEtat.Activee != false) {
            FEtat.Activee = false;
            FAllocation.Ecrire(FEtat);
        }
    }

    /**
     * @brief Retourne si la notification est active ou non
     *
     * @return Si la notification est active
     * @retval true La notification est active
     * @retval false La notification n'est pas active
     */
    bool EstActive() const {
        return FEtat.Activee;
    }

    /**
     * @brief Envoi la notification à chaque contact dans la liste données si la
     * notification est activée
     *
     */
    int Envoyer(
        //! Liste de contacts
        TContacts &contacts) {
        int codeErreur = 0;

        if (FEtat.Activee) {
            TContact *mContacts = contacts.Contacts();

            bool contactsModifies = false;
            for (size_t i = 0; i < NB_CONTACTS; i++) {
                TContact &contact = mContacts[i];
                if (contact.EstActif() &&
                    !contact.RaisonsEnvoyees.EstPresente(Raison)) {
                    // Envoyer le message.
                    Serial.printlnf("Nom: %s, Tel: %s, Msg: %s", contact.Nom,
                                    contact.NumTel, FMessage.c_str()); // TODO
                    Cellular.sendSms(contact.NumTel, FMessage.c_str());

                    // Considérer que le message est envoyé pour ce contact.
                    contact.RaisonsEnvoyees.Ajouter(Raison);

                    contactsModifies = true;
                }
            }

            if (contactsModifies) {
                contacts.Enregistrer();
            }
        }

        return codeErreur;
    }
};

#endif /* TNotification_H */
