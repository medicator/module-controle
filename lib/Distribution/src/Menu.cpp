/**
 * @file Menu.cpp
 * @brief Implémentation de TItemMenu, AfficherMenu, GererMenu et des fonctions
 * des items du menu
 */

#include "Menu.h"
#include <Particle.h>
#include <ctype.h> // isdigit, isalpha, islower, toupper

//! Taille maximale de la chaîne de caractère d'un jour de la semaine.
#define TAILLE_MAX_JOUR_SEMAINE 8

//! Taille maximum que peut faire le mot de passe pour déverrouiller
//! le module de distribution.
#define TAILLE_MDP_MAX 64

//! Mot de passe pour déverrouiller
//! le module de distribution.
static const char MDP[] = "le mot de passe";

// S'assurer que le mot de passe ne dépasse pas
// la taille maximale d'un mot de passe.
static_assert(sizeof(MDP) <= TAILLE_MDP_MAX,
              "Le mot de passe doit être inférieure ou égale à TAILLE_MDP_MAX");

//! Message d'erreur à ajouter à une erreur si elle n'est pas due à
//! l'utilisateur, mais qu'elle est due à une erreur des développeurs.
#define ERREUR_DEV                                                             \
    "Ceci est une erreur dans ce logiciel, "                                   \
    "veuillez la rapporter aux développeurs."

//! Messages d'erreur d'ajout de contact.
static const char *MSG_ERREUR_AJOUT_CONTACT[] = {
    "Le contact a été ajouté avec succès!",
    "Le paramètre nom est nul. " ERREUR_DEV,
    "Le paramètre numTel est nul. " ERREUR_DEV,
    "La liste de raisons est vide. Vous devez sélectionner au moins une "
    "raison.",
    "Le nom doit comporter au moins un caractère.",
    "Le numéro de téléphone doit comporter exactement" STRINGIFY(
        TAILLE_NUM_TEL) "caractères.",
    "Des caractères invalides se trouvent dans le nom donné. " ERREUR_DEV,
    "Des caractères invalides se trouvent dans le numéro de "
    "téléphone. " ERREUR_DEV,
    "La liste de contacts est pleine.",
    "Un contact avec le nom donné existe déjà.",
};
//! Nombre de messages d'erreur d'ajout de contact.
static const size_t NB_MSG_ERREUR_AJOUT_CONTACT =
    sizeof(MSG_ERREUR_AJOUT_CONTACT) / sizeof(const char *);

//! Messages d'erreur de modification de l'horaire.
static const char *MSG_ERREUR_MODIFIER_HORAIRE[] = {
    "L'horaire a été modifié avec succès!",
    "Le pointeur nouvelHoraire est nul. " ERREUR_DEV,
    "Le nb de médicaments d'une des distributions est de 0. " ERREUR_DEV,
    "L'heure d'une des distributions à l'horaire est plus grande "
    "que" STRINGIFY(NB_DISTRIBUTIONS_SEMAINE) ". " ERREUR_DEV,
    "La différence entre l'heure de deux distributions adjacentes est "
    "inférieure à " STRINGIFY(DELAI_DISTRIBUTIONS_MIN) " secondes",
};
//! Nombre de messages d'erreur de modification de l'horaire.
static const size_t NB_MSG_ERREUR_MODIFIER_HORAIRE =
    sizeof(MSG_ERREUR_MODIFIER_HORAIRE) / sizeof(const char *);

//! Raisons de contacter un contact.
static const char *RAISONS_CONTACT[] = {
    "Il est l'heure pour le patient de prendre ses médicaments", // rcMedicAPrendre
    "Le capteur de niveau du module indique que le niveau de médicaments "
    "est critique", // rcNiveauCritique
    "Le nombre de médicament dans le réservoir est inférieur au seuil "
    "critique", // rcNbCritique
    "Il ne reste plus assez de médicaments dans le réservoir pour donner "
    "sa dose complète au patient",                              // rcManqueMedic
    "Le patient est en retard sur la prise de ses médicaments", // rcDebutRetard
    "Le patient était en retard sur la prise de ses médicaments mais il "
    "vient finalement de les prendre",               // rcFinRetard
    "Le patient a raté la prise de ses médicaments", // rcPriseRatee
};

//==============================================================================
// Implémentation de TItemMenu
//==============================================================================

/**
 * @brief Initialise un item du menu
 *
 */
TItemMenu::TItemMenu(const char *nom, std::function<void(void)> selectionner) :
    Nom(nom), Selectionner(selectionner) {
}

//==============================================================================
// Fonctions pour lire des chaînes de caractères du port sériel et les convertir
//==============================================================================

/**
 * @brief Retourne si le caractère donné est un chiffre
 *
 * @return Si le caractère donné est un chiffre
 */
bool ValiderChiffre(
    //! Caractère à valider
    char c) {
    return isdigit(c) != 0;
}

/**
 * @brief Retourne si le caractère donné peut être contenu dans le nom d'un
 * contact
 * @note Les caractères permis dans le nom d'un contact sont a-z, a-Z, ' ', '-'
 *
 * @return Si le caractère donné est un chiffre
 */
bool ValiderCarNom(
    //! Caractère à valider
    char c) {
    return isalpha(c) || c == ' ' || c == '-';
}

/**
 * @brief Retourne si le caractère donné est une lettre minuscule
 * @note Les caractères permis dans le nom d'un contact sont a-z
 *
 * @return Si le caractère est une lettre minuscule
 */
bool ValiderAlphaMinuscule(
    //! Caractère à valider
    char c) {
    return isalpha(c) && islower(c);
}

/**
 * @brief Valide les caractères 'o' et 'n' en minuscule ou en majuscule
 *
 * @return Si le caractère est 'o' ou 'n' en minuscule ou en majuscule
 */
bool ValiderOuiNon(
    //! Caractère à valider
    char c) {
    return c == 'o' || c == 'O' || c == 'n' || c == 'N';
}

/**
 * @brief Valide tous les caractères en retournant toujours true
 *
 * @return Si le caractère est valide
 * @retval true Le caractère est valide
 */
bool ValiderTousCaracteres(char) {
    return true;
}

/**
 * @brief Lit la chaîne de caractères donnée
 *
 * @return Le nb de caractères lus ou un code d'erreur
 * @retval >=0 Le nb de caractères lus ou un code d'erreur
 * @retval -1 Le paramètre chaine est nul
 */
int32_t LireChaine(
    //! Chaîne de caractères
    //! Cette chaîne doit pouvoir contenir au moins nbCaracteresMax + 1
    char *chaine,
    //! Nombre maximum de caractères à lire
    size_t nbCaracteresMax,
    //! Fonction à appeler pour valider décider de garder ou non un caractère
    bool (&validerCaractere)(char c)) {
    int32_t resultat = 1;

    if (chaine != nullptr) {
        // Lire les caractères jusqu'à l'envoi d'un retour de chariot.
        int c = 0;    // Dernier caractère lu.
        size_t i = 0; // Index du prochain caractère à écrire dans le tampon.
        do {
            do {
                c = Serial.read();
            } while (c == -1);

            if (c == '\b' && i > 0) {
                i--;
                Serial.print("\b \b");
            } else if (i < nbCaracteresMax && c != '\r' && c != '\n' &&
                       validerCaractere(c)) {
                chaine[i] = c;
                i++;
                Serial.print((char)c);
            }
        } while (c != '\r' && c != '\n');
        Serial.println();
        chaine[i] = 0;

        // Ignorer tous les caractères non lus.
        while (Serial.read() != -1) {
        }

        resultat = i;
    }

    return resultat;
}

/**
 * @brief Lit un nombre entier positif du port sériel
 *
 * @return Le nombre lu ou un code d'erreur
 * @retval >0 Le nombre lu
 * @retval -1 Aucun caractère n'a été entré
 * @retval -2 Le nombre entier dépasse la limite de 2^32-1
 */
int32_t LireNbPositif() {
    static const size_t TAILLE_TAMPON_32 = 10;
    char tampon[TAILLE_TAMPON_32 + 1];

    int32_t nbCar = LireChaine(tampon, TAILLE_TAMPON_32, ValiderChiffre);
    int32_t resultat = -1;
    if (nbCar > 0) {
        if (sscanf(tampon, "%li", &resultat) != 1) {
            resultat = -2;
        }
    }
    return resultat;
}

//==============================================================================
// Fonctions pour gérer les interractions avec le menu
//==============================================================================

/**
 * @brief Affiche le menu et invite l'utilisateur à choisir un item
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Le paramètre items est nul
 */
int AfficherMenu(
    //! Liste d'items du menu
    const TItemMenu *items,
    //! Nombre d'items dans le menu
    size_t nbItems,
    //! Message d'erreur à afficher juste avant l'invite de la commande
    const char *messageErreur) {
    int codeErreur = 1;

    if (items != nullptr || nbItems == 0) {
        codeErreur = 0;

        // Afficher chaque item du menu avec son index.
        for (size_t i = 0; i < nbItems; i++) {
            Serial.printlnf("%i: %s", i + 1, items[i].Nom);
        }

        // Afficher le message d'erreur.
        if (messageErreur != nullptr) {
            Serial.println(messageErreur);
        }

        // Inviter l'utilisateur à sélectionner un item.
        Serial.println("Entrer le numéro de l'option que vous désirez choisir");
        Serial.print("> ");
    }

    return codeErreur;
}

/**
 * @brief Lorsque des caractères sont reçus sur le port sériel, cette méthode
 * détermine l'item du menu à sélectionner et le sélectione.
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Le paramètre items est nul
 */
int GererMenu(
    //! Liste d'items du menu
    const TItemMenu *items,
    //! Nombre d'items dans le menu
    size_t nbItems) {
    int codeErreur = 0;
    if (items == nullptr && nbItems != 0) {
        codeErreur = 1;
    } else if (Serial.available()) {
        const char *msgErreur = nullptr;
        int32_t choix = LireNbPositif();
        if (choix > 0 && (size_t)choix <= nbItems) {
            const TItemMenu &item = items[choix - 1];
            Serial.println();
            Serial.println(item.Nom);
            item.Selectionner();
            Serial.println();
        } else if (choix > (int32_t)nbItems || choix == -2) {
            msgErreur = "L'item sélectionné n'existe pas";
        }
        AfficherMenu(items, nbItems, msgErreur);
    }
    return codeErreur;
}

//==============================================================================
// Fonctions des items du menu
//==============================================================================

/**
 * @brief Affiche le nb de médicaments dans le réservoir sur le port sériel
 *
 */
void MenuAfficherNbMedic(
    //! Module de distribution duquel il faut afficher le nombre de médicaments.
    TModuleDistribution &module) {
    Serial.printlnf("Il reste %i médicaments dans le réservoir",
                    module.MedicReservoir());
}

/**
 * @brief Change le nb de médicaments dans le réservoir en demandant à
 * l'utilisateur de l'entrer sur le port sériel, puis l'affiche sur le port
 * sériel
 *
 */
void MenuChangerNbMedic(
    //! Module de distribution dont il faut changer le
    //! nombre de médicaments dans le réservoir.
    TModuleDistribution &module) {
    Serial.print("Nouveau nb de médicaments: ");
    int32_t nbMedic = LireNbPositif();
    if (nbMedic < 0 || nbMedic > UINT16_MAX) {
        Serial.println("Le nombre donné est invalide. Il doit être inférieur "
                       "ou égal à 65535.");
    } else if (nbMedic > 0) {
        module.MedicReservoir(nbMedic);
    }

    MenuAfficherNbMedic(module);
}

/**
 * @brief Affiche le nb de médicaments critique sur le port sériel
 *
 */
void MenuAfficherNbMedicCritique(
    //! Module de distribution dont il faut afficher le
    //! nombre de médicaments critique.
    TModuleDistribution &module) {
    Serial.printlnf("Le nb critique de médicaments est %i",
                    module.NbMedicCritique());
}

/**
 * @brief Change le nb de médicaments critique en demandant à
 * l'utilisateur de l'entrer sur le port sériel, puis l'affiche sur le port
 * sériel
 *
 */
void MenuChangerNbMedicCritique(
    //! Module de distribution dont il faut changer le
    //! nombre de médicaments critique.
    TModuleDistribution &module) {
    Serial.print("Nouveau nb de médicaments critique: ");
    int32_t nbMedic = LireNbPositif();
    if (nbMedic < 0 || nbMedic > UINT16_MAX) {
        Serial.println("Le nombre donné est invalide. Il doit être inférieur "
                       "ou égal à 65535.");
    } else if (nbMedic > 0) {
        module.NbMedicCritique(nbMedic);
    }

    MenuAfficherNbMedicCritique(module);
}

/**
 * @brief Affiche le délai de retard le port sériel
 *
 */
void MenuAfficherDelaiRetard(
    //! Module de distribution dont le délai de retard doit être affiché.
    TModuleDistribution &module) {
    Serial.printlnf("Le délai de retard est de %i secondes",
                    module.DelaiRetard());
}

/**
 * @brief Change le délai de retard en demandant à l'utilisateur de
 * l'entrer sur le port sériel, puis l'affiche sur le port sériel
 *
 */
void MenuChangerDelaiRetard(
    //! Module de distribution dont il faut changer le
    //! délai de retard
    TModuleDistribution &module) {
    Serial.print("Nouveau délai de retard (en secondes): ");
    int32_t nbMedic = LireNbPositif();
    if (nbMedic < 0) {
        Serial.println("Le nombre donné est invalide. Il doit être inférieur à "
                       "4294967296");
    } else if (nbMedic > 0) {
        module.DelaiRetard(nbMedic);
    }

    MenuAfficherDelaiRetard(module);
}

/**
 * @brief Affiche le nom du médicament du module sur le port sériel
 *
 */
void MenuAfficherNomMedic(
    //! Module de distribution dont il faut changer
    //! le délai de retard
    TModuleDistribution &module) {
    Serial.printlnf("Le nom du médicament est %s", module.NomMedic());
}

/**
 * @brief Change le nom du médicament en demandant à l'utilisateur de
 * l'entrer sur le port sériel, puis l'affiche sur le port sériel
 *
 */
void MenuChangerNomMedic(
    //! Module de distribution dont il faut changer
    //! le délai de retard
    TModuleDistribution &module) {
    Serial.print("Nouveau nom du médicament: ");
    char nouveauNom[TAILLE_NOM_MEDIC_MAX + 1];
    LireChaine(nouveauNom, TAILLE_NOM_MEDIC_MAX, ValiderAlphaMinuscule);
    module.NomMedic(nouveauNom);

    MenuAfficherNomMedic(module);
}

/**
 * @brief Déverrouille le module de distribution pendant une période demandée à
 * l'utilisateur
 *
 */
void MenuDeverrouiller(
    //! Module de distribution qu'il faut déverrouiller
    TModuleDistribution &module) {
    Serial.print("Entrer le mot de passe: ");
    char mdp[TAILLE_MDP_MAX];
    LireChaine(mdp, TAILLE_MDP_MAX, ValiderTousCaracteres);
    if (strcmp(mdp, MDP) == 0) {
        Serial.println("Mot de passe valide!");
        Serial.print("Délai de déverrouillage en secs [0-255]: ");
        int32_t sec = LireNbPositif();
        if (sec < 0) {
            sec = 0;
        }
        if (sec <= UINT8_MAX) {
            module.Deverrouiller(sec);
            if (sec == 0) {
                Serial.println("Le module a été verrouillé");
            } else {
                Serial.printlnf(
                    "Le module va rester déverrouillé pendant %i secondes",
                    (uint8_t)sec);
            }
        } else {
            Serial.println(
                "Le nombre de secondes de déverrouillage maximal est de "
                "255 secondes.");
        }
    } else {
        Serial.println("Mot de passe invalide!");
    }
}

/**
 * @brief Demande à l'utilisateur de placer ou d'enlever son doigt du capteur
 * d'empreintes digitales
 *
 */
void EnleverOuPlacerDoigt(
    //! Si l'utilisateur doit enlever son doigt du capteur
    bool enlever) {
    Serial.println(enlever ? "Enlever votre doigt" : "Placer votre doigt");
}

/**
 * @brief Permet à l'utilisateur d'enregistrer une nouvelle empreinte
 *
 */
void MenuEnregistrerEmpreinte(
    //! Capteur d'empreinte digitale
    TCapteurEmpreintes &capteur) {
    Serial.print("Index du doigt à enregistrer [1,10]: ");
    int32_t doigt = LireNbPositif();
    if (doigt >= 1 && doigt <= 10) {
        if (capteur.EnregistrerEmpreinte(doigt - 1, EnleverOuPlacerDoigt) ==
            0) {
            Serial.printlnf("L'empreinte #%i a été enregistrée avec succès",
                            doigt);
        } else {
            Serial.println("L'empreinte n'a pas pu être enregistrée. "
                           "Assurer vous que le capteur soit bien connecté et "
                           "de bien suivre les instructions.");
        }
    } else {
        Serial.println("L'index donné est invalide, il devrait être entre 1 et "
                       "10 inclusivement.");
    }
}

/**
 * @brief Efface une ou toutes les empreintes du capteur d'empreintes
 *
 */
void MenuSupprimerEmpreinte(
    //! Capteur d'empreinte digitale
    TCapteurEmpreintes &capteur) {
    Serial.println("Index du droit à supprimer ou 0 pour supprimer toutes les "
                   "empreintes [0,10]: ");
    int32_t doigt = LireNbPositif();
    if (doigt >= 1 && doigt <= 10) {
        if (capteur.EffacerEmpreinte(doigt - 1) == 0) {
            Serial.printlnf("L'empreinte #%i a été effacée avec succès.",
                            doigt);
        } else {
            Serial.println("L'empreinte n'a pas pu être supprimée. "
                           "Assurer vous que le capteur soit bien connecté.");
        }
    } else if (doigt == 0) {
        if (capteur.ReinitialiserEmpreintes() == 0) {
            Serial.printlnf(
                "Toutes les empreintes ont étés effacées avec succès.", doigt);
        } else {
            Serial.println("Toutes les empreintes n'ont pas pu être effacées. "
                           "Assurer vous que le capteur soit bien connecté.");
        }
    } else {
        Serial.println("L'index donné est invalide, il devrait être entre 1 et "
                       "10 inclusivement.");
    }
}

/**
 * @brief Affiche le nom de chaque contact de la liste donnée ainsi que l'index
 * du contact dans la liste
 *
 */
void MenuAfficherContacts(
    //! La liste de contacts
    const TContacts &contacts) {
    size_t nbContact = 0;
    for (size_t i = 0; i < NB_CONTACTS; i++) {
        const TContact &contact = contacts.Contacts()[i];
        if (contact.EstActif()) {
            Serial.printlnf("%s: %s", contact.Nom, contact.NumTel);
            nbContact++;
        }
    }

    if (nbContact == 0) {
        Serial.println("Il n'y a aucun contact");
    }
}

/**
 * @brief Ajoute un contact à la liste de contacts
 *
 */
void MenuAjouterContact(
    //! Liste à laquelle ajouter un contact.
    TContacts &contacts) {
    int codeErreur = 4;

    char tamponNom[TAILLE_NOM_CONTACT_MAX + 1];
    Serial.print("Nom: ");
    int32_t carLus =
        LireChaine(tamponNom, TAILLE_NOM_CONTACT_MAX, ValiderCarNom);
    if (carLus != 0) {
        char tamponTel[TAILLE_NUM_TEL + 1];
        Serial.print("Téléphone: ");
        carLus = LireChaine(tamponTel, TAILLE_NUM_TEL, ValiderChiffre);

        if (carLus == TAILLE_NUM_TEL) {
            Serial.println("Raisons de contacter ce contact");
            TListeRaisonsContact raisons;
            for (size_t rc = rcPremier; rc <= (size_t)rcDernier; rc++) {
                char tamponRaison[2];
                Serial.printf("%s [o/n]: ", RAISONS_CONTACT[rc]);
                LireChaine(tamponRaison, 1, ValiderOuiNon);
                char choix = tamponRaison[0];
                if (choix == 'o' || choix == 'O') {
                    raisons.Ajouter((TRaisonContact)rc);
                }
            }
            codeErreur = contacts.Ajouter(tamponNom, tamponTel, raisons);
        } else {
            codeErreur = 5;
        }
    }

    const char *msgErreur = "Une erreur inconnue est survenue. " ERREUR_DEV;
    if (codeErreur < (int)NB_MSG_ERREUR_AJOUT_CONTACT) {
        msgErreur = MSG_ERREUR_AJOUT_CONTACT[codeErreur];
    }
    Serial.println(msgErreur);
    if (codeErreur != 0) {
        Serial.println("Aucun contact n'a été ajouté.");
    }
}

/**
 * @brief Supprime un contact de la liste de contacts
 *
 */
void MenuSupprimerContact(
    //! Liste à laquelle ajouter un contact.
    TContacts &contacts) {
    // Afficher la liste de contacts avec des index.
    size_t nbContactsValides = 0;
    for (size_t i = 0; i < NB_CONTACTS; i++) {
        const TContact &contact = contacts.Contacts()[i];
        if (contact.EstActif()) {
            nbContactsValides++;
            Serial.printlnf("%i: %s, %s", nbContactsValides, contact.Nom,
                            contact.NumTel);
        }
    }

    if (nbContactsValides > 0) {
        Serial.println();
        Serial.printf("Index du contact à supprimer: [1,%i]: ",
                      nbContactsValides);
        int32_t index = LireNbPositif();
        if (index >= 1 && index <= (int32_t)nbContactsValides) {
            const char *nom = nullptr;
            size_t idxContact = 0;
            for (size_t i = 0; i < NB_CONTACTS && nom == nullptr; i++) {
                const TContact &contact = contacts.Contacts()[i];
                if (contact.EstActif()) {
                    idxContact++;
                    if (idxContact == nbContactsValides) {
                        nom = contact.Nom;
                    }
                }
            }

            Serial.println(
                contacts.Supprimer(nom) == 0
                    ? "Le contact a été supprimé avec succès"
                    : "Aucun contact avec le nom donné n'a pu être trouvé");
        } else {
            Serial.println("L'index donné est invalide.");
        }
    } else {
        Serial.println("Il n'y a aucun contact à supprimer.");
    }
}

/**
 * @brief Affiche toutes les heures de distribution de la liste
 *
 */
void MenuAfficherHoraireDistribution(
    //! Horaire de distribution à afficher
    const THoraireDistribution &horaire) {
    const size_t taille = horaire.NbDistributions();
    const THeureDistribution *distributions = horaire.Distributions();

    if (taille > 0) {
        Serial.println("Heure de la prise : nb de médicaments à prendre");
    }

    for (size_t i = 0; i < taille; i++) {
        const THeureDistribution &dist = distributions[i];

        uint8_t jourSemaine = dist.SecSemaine / (24 * 60 * 60);
        uint8_t heure = (dist.SecSemaine / (60 * 60)) % 24;
        uint8_t min = (dist.SecSemaine / 60) % 60;
        uint8_t sec = dist.SecSemaine % 60;
        Serial.printlnf("%s %02i:%02i:%02i : %i médicaments",
                        NOMS_JOURS_SEMAINE[jourSemaine], heure, min, sec,
                        dist.NbMedic);
    }

    if (taille == 0) {
        Serial.println("Aucune distribution n'est configurée.");
    }
}

/**
 * @brief Modifie l'horaire de distribution
 *
 */
void MenuModifierHoraireDistribution(
    //! Horaire de distribution à afficher
    THoraireDistribution &horaire) {
    MenuAfficherHoraireDistribution(horaire);

    // Lire le nouvel horaire.
    THeureDistribution nouvelHoraire[NB_DISTRIBUTIONS_SEMAINE];
    char tamponJourSemaine[TAILLE_MAX_JOUR_SEMAINE + 1];
    bool continuer = true;
    size_t i = 0;
    for (; i < NB_DISTRIBUTIONS_SEMAINE && continuer; i++) {
        Serial.printlnf("Distribution #%i", i + 1);

        int32_t nbMedic;
        uint8_t jour;
        uint8_t heure;
        uint8_t min;

        // Nombre de médicaments.
        Serial.print("Nb de médicaments à distribuer: ");
        nbMedic = LireNbPositif();

        // Jour de la distribution.
        if (nbMedic > 0 && nbMedic < UINT8_MAX) {
            Serial.println("Heure de la distribution: ");

            Serial.print("Jour de la semaine: ");
            LireChaine(tamponJourSemaine, TAILLE_MAX_JOUR_SEMAINE,
                       ValiderAlphaMinuscule);
            continuer = false;
            for (jour = 0; jour < NB_JOUR_SEMAINE && !continuer; jour++) {
                if (strcmp(NOMS_JOURS_SEMAINE[jour], tamponJourSemaine) == 0) {
                    continuer = true;
                }
            }
            jour--;
        } else {
            continuer = false;
        }

        // Heure.
        if (continuer) {
            Serial.print("Heure: ");
            int32_t nb = LireNbPositif();
            if (nb >= 0 && nb < 24) {
                heure = nb;
            } else {
                continuer = false;
            }
        }

        // Minute.
        if (continuer) {
            Serial.print("Minute: ");
            int32_t nb = LireNbPositif();
            if (nb >= 0 && nb < 60) {
                min = nb;
            } else {
                continuer = false;
            }
        }

        // Seconde.
        if (continuer) {
            Serial.print("Seconde: ");
            int32_t sec = LireNbPositif();
            if (sec >= 0 && sec < 60) {
                nouvelHoraire[i] =
                    THeureDistribution(nbMedic, jour, heure, min, sec);
            } else {
                continuer = false;
            }
        }
    }
    i--;

    // Modifier l'horaire.
    int codeErreur = horaire.Modifier(nouvelHoraire, i);

    // Afficher le message d'erreur.
    const char *msgErreur = "Une erreur inconnue est survenue";
    if (codeErreur >= 0 && codeErreur < (int)NB_MSG_ERREUR_MODIFIER_HORAIRE) {
        msgErreur = MSG_ERREUR_MODIFIER_HORAIRE[codeErreur];
    }
    Serial.println(msgErreur);

    if (i == 0) {
        Serial.println("L'horaire ne contient plus aucune distribution.");
    }
}

/**
 * @brief Affiche l'heure du système
 *
 */
void MenuAfficherHeureSysteme() {
    time_t heure = Time.local();
    struct tm tmHeure = *gmtime(&heure);

    const size_t TAILLE_TAMPON_DATE = 50;
    char tamponDate[TAILLE_TAMPON_DATE + 1];
    strftime(tamponDate, TAILLE_TAMPON_DATE, "%d/%m/%Y %H:%M:%S", &tmHeure);
    Serial.printlnf("%s %s", NOMS_JOURS_SEMAINE[tmHeure.tm_wday], tamponDate);
}

/**
 * @brief Change l'heure du système
 *
 */
void MenuChangerHeureSysteme() {
    // Afficher l'heure du système.
    MenuAfficherHeureSysteme();

    const size_t TAILLE_TAMPON_DATE = 50;
    char tamponDate[TAILLE_TAMPON_DATE + 1];
    Serial.println(
        "Entrer la date selon le format suivant: jj/mm/aaaa hh:mm:ss");
    Serial.print("> ");
    LireChaine(tamponDate, TAILLE_TAMPON_DATE, ValiderTousCaracteres);
    struct tm tmHeure;
    if (strptime(tamponDate, "%d/%m/%Y %H:%M:%S", &tmHeure) != nullptr) {
        time_t heure = mktime(&tmHeure);
        heure += (Time.now() - Time.local()); // Convertir en UTC.
        Time.setTime(heure);

        Serial.print("L'heure a été changée à: ");
        MenuAfficherHeureSysteme();
    } else {
        Serial.println("L'heure donnée n'a pas pu être décodée. Assurez-vous "
                       "qu'elle respecte bien le format.");
    }
}
