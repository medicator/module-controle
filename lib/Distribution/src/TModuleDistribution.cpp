/**
 * @file TModuleDistribution.cpp
 * @brief Implémentaiton de TModuleDistribution
 */

#include "TModuleDistribution.h"

//! Délai de retard par défaut
#define DELAI_RETARD_DEFAUT (10 * 60) // 10 min

//! Noms des jours de la semaine (en commençant par dimanche).
const char *NOMS_JOURS_SEMAINE[NB_JOUR_SEMAINE] = {
    "dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi",
};

//==============================================================================
// Formats des messages des notifications
//==============================================================================

//! Format du message pour signaler qu'il est l'heure d'une prise de médicament.
static const char *FMT_NOTIF_MEDIC_A_PRENDRE =
    "%s: Il est l'heure de prendre %i medicaments.";

//! Format du message de la notification envoyée lorsqu'il a manqué de
//! médicaments pour une distribution au client.
static const char *FMT_NOTIF_MANQUE_MEDIC =
    "%s: Comme il manquait des medicaments dans le reservoir, le patient n'a "
    "pas "
    "pu obtenir tous les médicaments dont il avait besoin pour cette "
    "distribution. "
    "Il devrait recevoir %i medicaments, mais il ne pourra en recevoir que %i.";

//! Format de la notification envoyée lorsque les capteurs de niveau dans le
//! réservoir indiquent que le niveau est bas.
static const char *FMT_NOTIF_NIVEAU_CRITIQUE =
    "%s: Le niveau de medicaments est critique.";

//! Format du message de la notification de critique.
static const char *FMT_NOTIF_NB_CRITIQUE =
    "%s: Le nombre de medicament est critique. "
    "Il reste %i medicaments et le nombre critique de medicaments est %i.";

//! Format du message de la notification du début du retard sur la prise de
//! médicament.
static const char *FMT_NOTIF_DEBUT_RETARD =
    "%s: Le patient aurait du prendre ses medicaments %s a %02i:%02i:%02i, "
    "mais il n'ont toujours pas ete pris.";

//! Format du message de la notification envoyée lorsque le patient était en
//! retard sur la prise de ses médicaments, mais qu'il vient finalement de les
//! prendre.
static const char *FMT_NOTIF_FIN_RETARD =
    "%s: Le patient vient finalement de prendre ses medicaments "
    "apres %uj %02i:%02i:%02i de retard.";

//! Format du message de la notification envoyée lorsque
//! le patient a raté la prise de ses médicaments.
static const char *FMT_NOTIF_PRISE_RATEE =
    "%s: Le patient a rate la prise des medicaments qu'il aurait du prendre "
    "depuis %s a %02i:%02i:%02i.";

/**
 * @brief Initialise le module de distribution
 * @note Il faut appeler begin avant n'importe quelle autre méthode de cette
 * classe une fois que l'objet a été initialisé
 *
 */
TModuleDistribution::TModuleDistribution(
    //! DEL à faire clignotter pour indiquer la disponibilité de
    //! médicaments.
    TRefDel<TDel> delMedicDispo,
    //! DEL à faire clignotter en cas d'erreur.
    TRefDel<TDelClignotteTemp> delErreur) :
    FDelMedicDispo(delMedicDispo),
    FDelErreur(delErreur),
    FEtat{
        0,                      // NbMedicCritique
        0,                      // MedicReservoir
        0,                      // MedicADistribuer
        "",                     // NomMedic
        THoraireDistribution(), // Horaire
        false,                  // NiveauEstCritique
        -1,                     // DerniereDistribution
        DELAI_RETARD_DEFAUT,    // DelaiRetard
        false                   // AttenteAuth
    },
    FEnregistrement(false), FEnvoiNbMedic(true),
    FNotifMedicAPrendre(FMT_NOTIF_MEDIC_A_PRENDRE, rcMedicAPrendre),
    FNotifManqueMedic(FMT_NOTIF_MANQUE_MEDIC, rcManqueMedic),
    FNotifNiveauCritique(FMT_NOTIF_NIVEAU_CRITIQUE, rcNiveauCritique),
    FNotifNbCritique(FMT_NOTIF_NB_CRITIQUE, rcNbCritique),
    FNotifDebutRetard(FMT_NOTIF_DEBUT_RETARD, rcDebutRetard),
    FNotifFinRetard(FMT_NOTIF_FIN_RETARD, rcFinRetard),
    FNotifPriseRatee(FMT_NOTIF_PRISE_RATEE, rcPriseRatee),
    EnvoyerNbMedic(nullptr), DeverrouillerRecharge(nullptr) {
}

/**
 * @brief Restaure l'état du module de distribution
 *
 */
void TModuleDistribution::begin(TGestionnaireEEPROM &gestionnaireEEPROM) {
    gestionnaireEEPROM.Allouer(sizeof(FEtat), FAllocation);
    if (FAllocation.Lire(FEtat) != 0) {
        FDelErreur.Del().FaireClignotter();
        Serial.println(
            "Impossible de restaurer l'état du module de distribution.");
    }

    // Restaurer les notifications.
    RestaurerNotification(FNotifMedicAPrendre, "médicaments à prendre",
                          gestionnaireEEPROM);
    RestaurerNotification(FNotifManqueMedic, "manque de médicaments",
                          gestionnaireEEPROM);
    RestaurerNotification(FNotifNiveauCritique,
                          "niveau critique de médicaments", gestionnaireEEPROM);
    RestaurerNotification(FNotifNbCritique, "nombre de médicament critique",
                          gestionnaireEEPROM);
    RestaurerNotification(FNotifDebutRetard, "début d'un retard du patient",
                          gestionnaireEEPROM);
    RestaurerNotification(FNotifFinRetard, "fin du retard du patient",
                          gestionnaireEEPROM);
    RestaurerNotification(FNotifPriseRatee, "prise de médicaments ratée",
                          gestionnaireEEPROM);

    // Restaurer les contacts.
    if (Contacts.Restaurer(gestionnaireEEPROM) != 0) {
        FDelErreur.Del().FaireClignotter();
        Serial.println("Impossible de restaurer la liste de contacts.");
    }
}

/**
 * @brief Gère la distribution et l'envoi de notifications
 *
 */
void TModuleDistribution::loop() {
    const THeureDistribution *dist = nullptr;
    FEtat.Horaire.Distribution(nullptr, &dist, nullptr);

    time_t heureDist = 0;
    if (dist != nullptr) {
        // Si la distribution a changée
        heureDist = dist->HeureSemaine();
        if (heureDist != FEtat.DerniereDistribution) {
            FEtat.DerniereDistribution = heureDist;

            // S'il restait des médicaments à distribuer
            if (FEtat.MedicADistribuer != 0) {
                // Activer la notif de prise de médicaments ratée.
                time_t t = Time.local();
                struct tm dateLocale = *gmtime(&t);
                FNotifPriseRatee.Activer(Contacts, FEtat.NomMedic,
                                         NOMS_JOURS_SEMAINE[dateLocale.tm_wday],
                                         dateLocale.tm_hour, dateLocale.tm_min,
                                         dateLocale.tm_sec);
            }

            // S'il ne reste plus assez de médicaments à distribuer
            if (FEtat.MedicReservoir < dist->NbMedic) {
                // Distribuer les médicaments restants.
                FEtat.MedicADistribuer = FEtat.MedicReservoir;

                // Activer la notification de manque de médicaments.
                FNotifManqueMedic.Activer(Contacts, FEtat.NomMedic,
                                          dist->NbMedic,
                                          FEtat.MedicADistribuer);
                FDelErreur.Del().FaireClignotter();
            } else {
                FEtat.MedicADistribuer = dist->NbMedic;
            }

            if (FEtat.MedicADistribuer != 0) {
                FNotifMedicAPrendre.Activer(Contacts, FEtat.NomMedic,
                                            FEtat.MedicADistribuer);
            }

            FEtat.AttenteAuth = FEtat.MedicADistribuer !=
                                0; // Activer l'attente de l'authentification si
                                   // c'est nécessaire.
            FEnvoiNbMedic = true;  // Activer l'envoi du nb de médic au module.

            // Désactiver la notification du début du retard.
            FNotifDebutRetard.Desactiver();

            FEnregistrement = true;
        }
    } else if (FEtat.MedicADistribuer != 0 || FEtat.AttenteAuth) {
        // Désactiver la distribution.
        FEtat.MedicADistribuer = 0;
        FEtat.AttenteAuth = false;
        FEnvoiNbMedic = true;
        FEnregistrement = true;
    }

    // Allumer le buzzer si on est en attente de l'authentification du
    // patient.
    TBuzzer::Etat(FEtat.AttenteAuth ? TBuzzer::CLIGNOTTE : TBuzzer::ETEINT);

    // Faire clignotter la DEL s'il y a des médicaments à distribuer.
    FDelMedicDispo.Del().Etat(FEtat.MedicADistribuer != 0 ? edClignotte
                                                          : edEteinte);

    // Envoyer le nb de médicament au module de distribution.
    if (FEnvoiNbMedic) {
        if (EnvoyerNbMedic != nullptr) {
            if (EnvoyerNbMedic(MedicADistribuer())) {
                FEnvoiNbMedic = false;
            } else {
                FDelErreur.Del().FaireClignotter();
                Serial.println("Impossible de changer le nb de médicaments du "
                               "module de distribution");
            }
        } else {
            FDelErreur.Del().FaireClignotter();
            Serial.println(
                "La fonction pour envoyer le nb de médicaments au module "
                "de distribution n'a pas été configurée");
        }
    }

    // Envoyer la notification de prise ratée.
    FNotifPriseRatee.Envoyer(Contacts);

    // Envoyer la notification de distribution.
    FNotifMedicAPrendre.Envoyer(Contacts);

    // Envoyer la notification de manque de médicaments.
    FNotifManqueMedic.Envoyer(Contacts);

    // S'il reste des médicaments à distribuer et
    // qu'on est en retard sur leur distribution.
    if (FEtat.MedicADistribuer != 0 &&
        (uint32_t)(Time.local() - FEtat.DerniereDistribution) >=
            FEtat.DelaiRetard) {
        // Activer la notification de début du retard.
        if (!FNotifDebutRetard.EstActive()) {
            struct tm dateLocale = *gmtime(&heureDist);
            FNotifDebutRetard.Activer(Contacts, FEtat.NomMedic,
                                      NOMS_JOURS_SEMAINE[dateLocale.tm_wday],
                                      dateLocale.tm_hour, dateLocale.tm_min,
                                      dateLocale.tm_sec);
        }

        // Envoyer la notification du début du retard.
        FNotifDebutRetard.Envoyer(Contacts);
    } else if (FNotifDebutRetard.EstActive()) {
        // Activer la notification de fin du retard.
        uint32_t diff = Time.local() - heureDist; // Calculer le retard.
        uint32_t jour = diff / (24 * 60 * 60);
        uint8_t heure = (diff / (60 * 60)) % 24;
        uint8_t min = (diff / 60) % 60;
        uint8_t sec = diff % 60;
        FNotifFinRetard.Activer(Contacts, FEtat.NomMedic, jour, heure, min,
                                sec);

        // Désactiver la notification du début du retard.
        FNotifDebutRetard.Desactiver();
    }

    // Envoyer la notification de fin du retard.
    FNotifFinRetard.Envoyer(Contacts);

    // Notif de niveau critique
    if (FEtat.NiveauEstCritique) {
        // Activer la notification.
        if (!FNotifNiveauCritique.EstActive()) {
            FNotifNiveauCritique.Activer(Contacts, FEtat.NomMedic);
        }
        // Envoyer la notification.
        FNotifNiveauCritique.Envoyer(Contacts);
    } else {
        FNotifNiveauCritique.Desactiver();
    }

    // Notif de nb de médicament critique
    if (FEtat.MedicReservoir <= FEtat.NbMedicCritique) {
        // Activer la notification.
        if (!FNotifNbCritique.EstActive()) {
            FNotifNbCritique.Activer(Contacts, FEtat.NomMedic,
                                     FEtat.MedicReservoir,
                                     FEtat.NbMedicCritique);
        }
        // Envoyer la notification.
        FNotifNbCritique.Envoyer(Contacts);
    } else {
        FNotifNbCritique.Desactiver();
    }

    // Enregistrer l'état s'il a changé.
    if (FEnregistrement) {
        if (FAllocation.Ecrire(FEtat) == 0) {
            FEnregistrement = false;
        } else {
            FDelErreur.Del().FaireClignotter();
            Serial.println("L'état du distributeur n'a pas pu être enregistré");
        }
    }
}

/**
 * @brief Restaure la notification donnée du EEPROM
 *
 * @tparam Props Propriétés de la notification
 */
template <typename... Props>
void TModuleDistribution::RestaurerNotification(
    //! Notification à restaurer.
    TNotification<Props...> &notif,
    //! Message associé à l'erreur de la notification.
    //! Il est affiché sur le port sériel précédé de
    //! "Impossible de restaurer la notification de ".
    const char *msg,
    //! Gestionnaire du EEPROM.
    TGestionnaireEEPROM &gestionnaireEEPROM) {
    if (notif.Restaurer(gestionnaireEEPROM) != 0) {
        FDelErreur.Del().FaireClignotter();
        Serial.printlnf("Impossible de restaurer la notification de %s.", msg);
    }
}

/**
 * @brief Retourne si un médicament doit être distribué par le module de
 * contrôle et retire un médicament des médicaments à distribuer et du
 * réservoir si c'est le cas
 *
 * @return Si un médicament peut être distribué par le module de
 * distribution
 * @retval true Un médicament peut être distribué par le module de
 * distribution
 * @retval true Aucun médicament ne doit être distribué par le module de
 * distribution
 */
bool TModuleDistribution::DistribuerMedic() {
    bool resultat = MedicADistribuer() > 0;
    if (resultat) {
        FEtat.MedicReservoir--;
        FEtat.MedicADistribuer--;
        FEnregistrement = true;
    }
    return resultat;
}

/**
 * @brief Change si le niveau de médicament est critique ou non
 *
 */
void TModuleDistribution::NiveauMedicCritique(
    //! Si le niveau de médicament est critique dans le réservoir
    bool estCritique) {
    if (FEtat.NiveauEstCritique != estCritique) {
        FEtat.NiveauEstCritique = estCritique;
        FEnregistrement = true;
    }
}

/**
 * @brief Authentifie l'utilisateur et active la distribution de médicaments
 *
 * @return Si l'authentification était attendue
 * @retval true L'authentification n'était pas attendue
 * @retval false L'authentification n'était pas attendue
 */
bool TModuleDistribution::Authentifier() {
    const bool resultat = FEtat.AttenteAuth;

    if (FEtat.AttenteAuth) {
        // Activer l'envoi du nb de médicaments.
        FEnvoiNbMedic = true;

        // Désactiver l'attente de notification.
        FEtat.AttenteAuth = false;

        FEnregistrement = true;
    }

    return resultat;
}

/**
 * @brief Déverrouille la recharge de médicaments pendant un certain nombre
 * de seconds
 *
 * @return Un code d'erreur
 * @see TCommunication::DeverrouillerRecharge
 */
int TModuleDistribution::Deverrouiller(
    //! Nombre de secondes de déverrouillage du module.
    uint8_t sec) {
    int codeErreur = -1;
    if (DeverrouillerRecharge != nullptr) {
        codeErreur = DeverrouillerRecharge(sec);
    }
    return codeErreur;
}

/**
 * @brief Retourne l'horaire de distribution
 *
 * @return L'horaire de distribution
 */
THoraireDistribution &TModuleDistribution::Horaire() {
    return FEtat.Horaire;
}

/**
 * @brief Retourne l'horaire de distribution
 *
 * @return L'horaire de distribution
 */
const THoraireDistribution &TModuleDistribution::Horaire() const {
    return FEtat.Horaire;
}

/**
 * @brief Retourne le nom du médicament contenu dans le distributeur
 *
 * @return Le nom du médicament
 */
const char *TModuleDistribution::NomMedic() {
    return FEtat.NomMedic;
}

/**
 * @brief Change le nom du médicament contenu dans le distributeur
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur n'est survenue
 * @retval 1 Le pointeur donné est nul
 */
int TModuleDistribution::NomMedic(
    //! Nouveau nom du médicament
    const char *nom) {
    int codeErreur = 1;
    if (nom != nullptr) {
        codeErreur = 0;
        if (strcmp(nom, FEtat.NomMedic) != 0) {
            strncpy(FEtat.NomMedic, nom, TAILLE_NOM_MEDIC_MAX);
            FEnregistrement = true;
        }
    }
    return codeErreur;
}

/**
 * @brief Retourne le nombre de médicaments à distribuer
 *
 * @return Le nombre de médicaments à distribuer
 */
uint8_t TModuleDistribution::MedicADistribuer() const {
    return FEtat.AttenteAuth ? 0 : FEtat.MedicADistribuer;
}

/**
 * @brief Retourne le nombre de médicaments dans le réservoir
 *
 * @return Le nombre de médicaments dans le réservoir
 */
uint16_t TModuleDistribution::MedicReservoir() const {
    return FEtat.MedicReservoir;
}

/**
 * @brief Change le nombre de médicaments dans le réservoir et réinitialise
 * le nombre de médicaments à distribuer.
 *
 */
void TModuleDistribution::MedicReservoir(
    //! Nouveau nb de médicaments dans le réservoir
    uint16_t medicReservoir) {
    if (FEtat.MedicReservoir != medicReservoir) {
        FEtat.MedicReservoir = medicReservoir;
        FEtat.MedicADistribuer = 0;
        FEnvoiNbMedic = true;
        FEnregistrement = true;
    }
}

/**
 * @brief Retourne le nombre de médicaments critique
 * @note Nombre de médicaments dans le réservoir à partir duquel
 * il est considéré comme critique et une notification doit être envoyée à ce
 * sujet.
 *
 * @return Le nombre de médicaments critique
 */
uint16_t TModuleDistribution::NbMedicCritique() const {
    return FEtat.NbMedicCritique;
}

/**
 * @brief Change le nombre de médicament critique
 * @note Nombre de médicaments dans le réservoir à partir duquel
 * il est considéré comme critique et une notification doit être envoyée à ce
 * sujet.
 *
 */
void TModuleDistribution::NbMedicCritique(
    //! Nouveau nombre de médicaments critique
    uint16_t nbCritique) {
    if (FEtat.NbMedicCritique != nbCritique) {
        FEtat.NbMedicCritique = nbCritique;
        FEnregistrement = true;
    }
}

/**
 * @brief Retourne le délai de retard en secondes
 *
 * @return Le délai de retard en secondes
 */
uint32_t TModuleDistribution::DelaiRetard() const {
    return FEtat.DelaiRetard;
}

/**
 * @brief Change le délai de retard pour celui donné
 *
 */
void TModuleDistribution::DelaiRetard(
    //! Nouveau délai de retard en secondes
    uint32_t delaiRetard) {
    if (FEtat.DelaiRetard != delaiRetard) {
        FEtat.DelaiRetard = delaiRetard;
        FEnregistrement = true;
    }
}
