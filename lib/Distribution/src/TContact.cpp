/**
 * @file TContact.cpp
 * @brief Implémentation de TListeRaisonsContact, TContact et TContacts
 */

#include "TContact.h"

//==============================================================================
// Implémentation de TListeRaisonsContact
//==============================================================================

/**
 * @brief Initialise une liste vide
 *
 */
TListeRaisonsContact::TListeRaisonsContact() : TListeRaisonsContact(0){};
/**
 * @brief Initialise une liste avec la valeur suivante
 *
 */
TListeRaisonsContact::TListeRaisonsContact(
    //! Valeur de la liste
    //! Ceci représente toutes les valeurs superposées
    uint32_t valeur) {
    // Filtrer les valeurs invalides.
    FValeur = valeur & VALEUR_PLEINE;
};

/**
 * @brief Retourne la valeur de la liste
 *
 * @return La valeur de la liste
 */
uint32_t TListeRaisonsContact::Valeur() const {
    return FValeur;
}

/**
 * @brief Ajoute la raison donnée à la liste
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 La raison donnée est invalide
 */
int TListeRaisonsContact::Ajouter(
    //! Raison à ajouter
    TRaisonContact raison) {
    int codeErreur = 1;
    if (RaisonValide(raison)) {
        codeErreur = 0;
        FValeur |= 1 << raison;
    }
    return codeErreur;
}

/**
 * @brief Retire la raison donnée de la liste
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 La raison donnée est invalide
 */
int TListeRaisonsContact::Retirer(
    //! Raison à retirer
    TRaisonContact raison) {
    int codeErreur = 1;
    if (RaisonValide(raison)) {
        codeErreur = 0;
        FValeur &= ~(1 << raison);
    }
    return codeErreur;
}

/**
 * @brief Retourne si la raison donnée fait partie de la liste
 *
 * @return Si la raison donnée fait partie de la liste
 * @retval true La raison donnée fait partie de la liste
 * @retval false La raison donnée est invalide ou ne faite pas partie de la
 * liste
 */
bool TListeRaisonsContact::EstPresente(
    //! Raison à tester
    TRaisonContact raison) const {
    bool resultat = false;
    if (RaisonValide(raison)) {
        resultat = (FValeur & (1 << raison)) > 0;
    }
    return resultat;
}

/**
 * @brief Retourne si la liste est pleine
 *
 * @return Si la liste est pleine
 * @retval true La liste est pleine
 * @retval false La liste n'est pas pleine
 */
bool TListeRaisonsContact::EstPleine() const {
    return FValeur == VALEUR_PLEINE;
}

/**
 * @brief Retourne si la liste est vide
 *
 * @return Si la liste est vide
 * @retval true La liste est vide
 * @retval false La liste n'est pas vide
 */
bool TListeRaisonsContact::EstVide() const {
    return FValeur == 0;
}

//==============================================================================
// Implémentation de TContact
//==============================================================================

/**
 * @brief Initialise un contact sans nom, ni numéro de téléphone et avec
 *
 */
TContact::TContact() : RaisonsContact(0), RaisonsEnvoyees(~0) {
    Nom[0] = 0;
    NumTel[0] = 0;
}

/**
 * @brief Retourne si ce contact est actif en vérifiant si sa liste de
 * raisons de contact est vide
 *
 * @return Si ce contact est actif
 * @retval true Ce contact est actif
 * @retval false Ce contact n'est pas actif
 */
bool TContact::EstActif() const {
    return !RaisonsContact.EstVide();
}

/**
 * @brief Désactive le contact en vidant sa liste de raisons de contact
 *
 */
void TContact::Desactiver() {
    RaisonsContact = TRaisonContact();
}

/**
 * @brief Active ce contact en lui donnant les propriétés suivantes
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Le paramètre nom est nul
 * @retval 2 Le paramètre numTel est nul
 * @retval 3 La liste de raisons est vide. Un contact avec une liste de
 * raisons vide est un contact désactivé.
 * @retval 4 La taille de la chaîne nom est trop grande ou trop petite
 * @retval 5 La taille de la chaîne numTel ne fait pas exactement TAILLE_NUM_TEL
 * @retval 6 Des caractères invalides se trouvent dans le nom donné
 * @retval 7 Des caractères invalides se trouvent dans le numéro de
 * téléphone
 */
int TContact::Activer(
    //! Nom de ce contact.
    //! Il doit seulement contenir des caractères parmi les suivants: ['a',
    //! 'Z'], '' et ' '.
    //! Sa taille maximale doit être de TAILLE_NOM_CONTACT_MAX.
    const char *nom,
    //! Numéro de téléphone de ce contact.
    //! Il doit seulement contenir des chiffres
    //! et doit être exactement de taille TAILLE_NUM_TEL.
    const char *numTel,
    //! Liste de raisons de contacter ce contact.
    //! Cette liste ne doit pas être vide.
    TListeRaisonsContact raisons) {
    int codeErreur = 0;

    // Valider le nom donné.
    if (nom == nullptr) {
        codeErreur = 1;
    } else if (strlen(nom) > TAILLE_NOM_CONTACT_MAX) {
        codeErreur = 4;
    } else {
        size_t i = 0;
        char c;
        do {
            c = nom[i];
            if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '-' ||
                c == ' ') {
                i++;
            } else if (c != 0) {
                codeErreur = 6;
            }
        } while (c != 0 && codeErreur == 0);
    }

    // Valide le numéro de téléphone donné.
    if (codeErreur == 0) {
        if (numTel == nullptr) {
            codeErreur = 2;
        } else if (strlen(numTel) != TAILLE_NUM_TEL) {
            codeErreur = 5;
        } else {
            size_t i = 0;
            char c;
            do {
                c = numTel[i];
                if (c >= '0' && c <= '9') {
                    i++;
                } else if (c != 0) {
                    codeErreur = 7;
                }
            } while (c != 0 && codeErreur == 0);
        }
    }

    // Valider la liste de raisons.
    if (codeErreur == 0) {
        if (raisons.EstVide()) {
            codeErreur = 3;
        }
    }

    // Si tous les paramètres donnés sont valides.
    if (codeErreur == 0) {
        // Activer le contact.
        strcpy(Nom, nom);
        strcpy(NumTel, numTel);
        RaisonsContact = raisons;
        RaisonsEnvoyees =
            TRaisonContact(~0); // Réinitialiser les raisons de notification.
    }

    return codeErreur;
}

//==============================================================================
// Implémentation de TContacts
//==============================================================================

/**
 * @brief Restaurer la liste de contacts du EEPROM
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval * Voir TAllocationEEPROM::Ecrire
 */
int TContacts::Restaurer(
    //! Gestionnaire du EEPROM
    TGestionnaireEEPROM &gestionnaireEEPROM) {
    gestionnaireEEPROM.Allouer(sizeof(FContacts), FAllocation);
    return FAllocation.Lire(FContacts);
}

/**
 * @brief Enregistre la liste de contacts
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval * Voir TAllocationEEPROM::Ecrire
 */
int TContacts::Enregistrer() {
    return FAllocation.Ecrire(FContacts);
}

/**
 * @brief Ajoute un contact avec les informations données à la liste
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Le paramètre nom est nul
 * @retval 2 Le paramètre numTel est nul
 * @retval 3 La liste de raisons est vide. Un contact avec une liste de
 * raisons vide est un contact désactivé.
 * @retval 4 La taille de la chaîne nom est trop grande ou trop petite
 * @retval 5 La taille de la chaîne numTel ne fait pas exactement TAILLE_NUM_TEL
 * @retval 6 Des caractères invalides se trouvent dans le nom donné
 * @retval 7 Des caractères invalides se trouvent dans le numéro de
 * téléphone
 * @retval 8 La liste de contacts est pleine
 * @retval 9 Un contact avec le nom donné existe déjà
 * @retval 10 Le contact a été ajouté, mais la liste de contacts n'a pas pue
 * être enregistrée
 */
int TContacts::Ajouter(
    //! Nom de ce contact.
    //! Il doit seulement contenir des caractères parmi les suivants: ['a',
    //! 'Z'], '' et ' '.
    //! Sa taille maximale doit être de TAILLE_NOM_CONTACT_MAX.
    const char *nom,
    //! Numéro de téléphone de ce contact.
    //! Il doit seulement contenir des chiffres
    //! et doit être exactement de taille TAILLE_NUM_TEL.
    const char *numTel,
    //! Liste de raisons de contacter ce contact.
    //! Cette liste ne doit pas être vide.
    TListeRaisonsContact raisons) {
    int codeErreur = 1;

    TContact *contactVide = nullptr;
    if (nom != nullptr) {
        codeErreur = 0;

        // Trouver un contact inutilisé et s'assurer qu'aucun contact n'a le
        // même nom que le contact donné.
        for (size_t i = 0; i < NB_CONTACTS && codeErreur == 0; i++) {
            TContact &contact = FContacts[i];
            if (!contact.EstActif()) {
                contactVide = &contact;
            } else if (strcmp(nom, contact.Nom) == 0) {
                codeErreur = 9;
            }
        }
    }

    // Si la liste de contacts est pleine.
    if (contactVide == nullptr) {
        codeErreur = 8;
    }
    // Sinon
    else if (codeErreur == 0) {
        // Ajouter le contact.
        codeErreur = contactVide->Activer(nom, numTel, raisons);
        if (codeErreur == 0) {
            if (Enregistrer() != 0) {
                codeErreur = 10;
            }
        }
    }
    return codeErreur;
}

/**
 * @brief Supprime le contact avec le nom donné
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Le nom donné est nul
 * @retval 2 Aucun contact avec le nom donné n'a pu être trouvé
 * @retval 3 Le contact a pu être supprimé, mais la liste de contacts
 * n'a pas pu être enregistrée
 */
int TContacts::Supprimer(
    //! Nom du contact à supprimer
    const char *nom) {
    int codeErreur = 1;
    if (nom != nullptr) {
        // Trouver le contact avec le nom donné et le désactiver.
        codeErreur = 2;
        for (int i = 0; i < NB_CONTACTS && codeErreur != 0; i++) {
            TContact &contact = FContacts[i];
            if (contact.EstActif() && strcmp(contact.Nom, nom) == 0) {
                contact.Desactiver();
                codeErreur = Enregistrer() == 0 ? 0 : 3;
            }
        }
    }
    return codeErreur;
}

/**
 * @brief Retourne la liste de contacts
 *
 * @return La liste de contacts
 */
TContact *TContacts::Contacts() {
    return FContacts;
}

/**
 * @brief Retourne la liste de contacts
 *
 * @return La liste de contacts
 */
const TContact *TContacts::Contacts() const {
    return FContacts;
}

/**
 * @brief Retourne le Nième contact actif
 *
 * @return Le Nième contact actif
 * @retval nullptr Il y a moins de n contacts actifs
 * @retval * Le pointeur vers le contact actif
 */
const TContact *TContacts::Contact(
    //! Index du contact actif
    size_t n) const {
    size_t i = 0;
    size_t nbActifs = 0;
    const TContact *contact;
    do {
        contact = &FContacts[i];
        if (contact->EstActif()) {
            nbActifs++;
        }
        i++;
    } while (nbActifs <= n && i < NB_CONTACTS);

    if (nbActifs <= n) {
        contact = nullptr;
    }

    return contact;
}
