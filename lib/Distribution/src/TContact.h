/**
 * @file TContact.h
 * @brief Déclaration de TRaisonContact, TListeRaisonsContact, TContact,
 * TListeContacts
 */

#ifndef TContact_H
#define TContact_H

#include <TGestionnaireEEPROM.h>
#include <stdint.h> // uint*_t
#include <string.h> // size_t

//! Taille maximale du nom d'un contact
#define TAILLE_NOM_CONTACT_MAX 32

//! Taille du numéro de téléphone d'un contact.
#define TAILLE_NUM_TEL 11

//! Nombre de contacts maximum supporté.
#define NB_CONTACTS 5

//! Raisons de contacter un contact.
enum TRaisonContact {
    //! Il est l'heure pour le patient de prendre ses médicaments.
    rcMedicAPrendre,

    //! Le capteur de niveau du module indique que le niveau de médicaments est
    //! critique.
    rcNiveauCritique,
    //! Le nombre de médicament dans le réservoir est inférieur au seuil
    //! critique.
    rcNbCritique,
    //! Il ne reste plus assez de médicaments dans le réservoir pour donner sa
    //! dose complète au patient.
    rcManqueMedic,

    //! Le patient est en retard sur la prise de ses médicaments.
    rcDebutRetard,
    //! Le patient était en retard sur la prise de ses médicaments,
    //! mais il vient finalement de les prendre.
    rcFinRetard,
    //! Le patient a raté la prise de ses médicaments.
    rcPriseRatee,

    rcPremier = rcMedicAPrendre,
    rcDernier = rcPriseRatee,
};

//! Liste de raisons de contact
class TListeRaisonsContact {
private:
    //! Valeur de la liste.
    //! L'index de chaque bit activé représente la valeur
    //! d'une variante de TRaisonContact faisant partie de cette liste.
    uint32_t FValeur;

public:
    //! Valeur de la liste lorsqu'elle est pleine.
    static const uint32_t VALEUR_PLEINE = ~(uint32_t)0 >>
                                          (32 - (rcDernier + rcPremier + 1))
                                              << rcPremier;

    TListeRaisonsContact();
    TListeRaisonsContact(uint32_t valeur);

    uint32_t Valeur() const;

    int Ajouter(TRaisonContact raison);
    int Retirer(TRaisonContact raison);

    bool EstPresente(TRaisonContact raison) const;

    bool EstPleine() const;
    bool EstVide() const;

    /**
     * @brief Retourne si la valeur donnée est une variante de
     * TRaisonContact
     *
     * @return Si la valeur donnée est une variante de TRaisonContact
     * @retval true La valeur donnée est une variante de TRaisonContact
     * @retval false La valeur donnée n'est pas une variante de
     * TRaisonContact
     */
    static inline bool RaisonValide(
        //! Valeur à vérifier
        TRaisonContact raison) {
        return raison >= rcPremier && raison <= rcDernier;
    }
};

//! Informations sur une personne à contacter et sur les raisons pour lesquelles
//! la contacter
struct TContact {
    //! Nom du contact.
    char Nom[TAILLE_NOM_CONTACT_MAX + 1];
    //! Numéro de téléphone à contacter.
    char NumTel[TAILLE_NUM_TEL + 1];

    //! Liste de raisons valides de contacter ce contact.
    //! Si ceci est à 0, ce contact n'a jamais besoin d'être contacter
    TListeRaisonsContact RaisonsContact;

    //! Liste des raisons des notifications qui ont été envoyées avec succès.
    //! Si une raison est dans cette liste, cela signifie qu'elle a été envoyée
    //! avec succès.
    //! Sinon, la notification n'a pas encore été envoyée ou une erreur est
    //! survenue.
    TListeRaisonsContact RaisonsEnvoyees;

    TContact();

    bool EstActif() const;

    int Activer(const char *nom, const char *numTel,
                TListeRaisonsContact raisons);
    void Desactiver();
};

//! Gère la liste de contacts et l'envoi de notifications à ces derniers.
class TContacts {
private:
    //! Liste de contacts.
    TContact FContacts[NB_CONTACTS];

    //! Allocation dans le EEPROM pour enregistrer la liste de contacts.
    TAllocationEEPROM FAllocation;

public:
    int Restaurer(TGestionnaireEEPROM &gestionnaireEEPROM);
    int Enregistrer();

    int Ajouter(const char *nom, const char *numTel,
                TListeRaisonsContact raisons);
    int Supprimer(const char *nom);

    TContact *Contacts();
    const TContact *Contacts() const;

    const TContact *Contact(size_t n) const;
};

#endif /* TContact_H */
