/**
 * @file THoraireDistribution.h
 * @brief Déclaration de THeureDistribution et THoraireDistribution
 */

#ifndef THoraireDistribution_H
#define THoraireDistribution_H

#include <Particle.h> // Time
#include <stdint.h>   // uint8_*
#include <string.h>   // size_t
#include <time.h>     // time_t

//! Nombre de secondes par semaine
#define NB_SECONDES_SEMAINE (7 * 24 * 60 * 60)

//! Nombre maximal de distributions en une semaine
#define NB_DISTRIBUTIONS_SEMAINE 35

//! Nombre de secondes minimal entre les distributions
#define DELAI_DISTRIBUTIONS_MIN 60 // 5min // TODO

//! Heure à laquelle distribuer des médicaments.
//! Si NbMedic est à 0, il n'y a pas de médicaments à distribuer à cette heure.
struct THeureDistribution {
    //! Nombre de médicaments à distribuer à cette heure.
    uint8_t NbMedic;
    //! Nombre de secondes depuis le début de la journée à laquelle il faut
    //! effectuer une distribution de médicaments.
    uint32_t SecSemaine;

    THeureDistribution();
    THeureDistribution(uint8_t nbMedic, uint32_t secSemaine);
    THeureDistribution(uint8_t nbMedic, uint8_t jour, uint8_t heure,
                       uint8_t min, uint8_t sec);

    bool EstActif() const;

    time_t HeureSemaine(const int position = 1) const;

    /**
     * @brief Retourne le nombre de secondes écoulées depuis le début de la
     * semaine pour l'heure locale actuelle
     *
     * @return Le nombre de secondes écoulées depuis le début de la semaine
     */
    static inline uint32_t NbSecondesSemaine() {
        return NbSecondesSemaine(Time.local());
    }
    /**
     * @brief Retourne le nombre de secondes écoulées depuis le début de la
     * semaine pour l'heure locale donnée
     *
     * @return Le nombre de secondes écoulées depuis le début de la semaine
     */
    static inline uint32_t NbSecondesSemaine(
        //! Heure pour laquelle calculer le nb de secondes
        time_t t) {
        struct tm heureLocale = *gmtime(&t);
        return NbSecondesSemaine(heureLocale.tm_wday, heureLocale.tm_hour,
                                 heureLocale.tm_min, heureLocale.tm_sec);
    }
    /**
     * @brief Retourne le nombre de secondes écoulées pour le nombre de jours,
     * heure, minute et secondes depuis le début de la semaine
     *
     * @return Le nombre de secondes écoulées depuis le début de la semaine
     */
    static inline uint32_t NbSecondesSemaine(
        //! Nombre de jours depuis dimanche
        uint8_t jourSemaine,
        //! Heure
        uint8_t heure,
        //! Minute
        uint8_t min,
        //! Seconde
        uint8_t sec) {
        return (((uint32_t)jourSemaine * 24 + heure) // Nb d'heures totales
                    * 60 +
                min) // Nb de minutes totales
                   * 60 +
               sec; // Nb de secondes totales.
    }
};

//! Horaire de distribution de médicaments sur une semaine avec
//! jusqu'à 35 distributions de médicaments par jour.
class THoraireDistribution {
private:
    //! Horaire de distribution.
    THeureDistribution FHoraire[NB_DISTRIBUTIONS_SEMAINE];
    //! Nombre d'heures de distribution active dans l'horaire.
    size_t FTaille;

public:
    THoraireDistribution();

    void Distribution(const THeureDistribution **precedente,
                      const THeureDistribution **enCours,
                      const THeureDistribution **suivante) const;

    int Modifier(const THeureDistribution *nouvelHoraire, int nbDist = -1);

    const THeureDistribution *Distributions() const;
    size_t NbDistributions() const;
};

#endif /* THoraireDistribution_H */
