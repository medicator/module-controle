/**
 * @file THoraireDistribution.cpp
 * @brief Implémentation de THeureDistribution et THoraireDistribution
 */

#include "THoraireDistribution.h"

/**
 * @brief Initialise une heure de distribution
 * sans aucun médicament à distribuer
 *
 */
THeureDistribution::THeureDistribution() : NbMedic(0), SecSemaine(0) {
}

/**
 * @brief Initialise une heure de distribution avec le nb de secondes depuis
 * le début de la semaine et le nb de medic donnés
 *
 */
THeureDistribution::THeureDistribution(
    //! Nombre de médicaments à distribuer
    uint8_t nbMedic,
    //! Nombre de secondes dans la semaine
    uint32_t secSemaine) :
    NbMedic(nbMedic),
    SecSemaine(secSemaine % NB_SECONDES_SEMAINE) {
}

/**
 * @brief Initialise une heure de distribution avec le nb de médicaments à
 * distribuer donné et le résultat du calcul du nombre de secondes écoulé depuis
 * le début de la semaine avec les autres paramètres donnés
 *
 */
THeureDistribution::THeureDistribution(
    //! Nombre de médicaments à distribuer
    uint8_t nbMedic,
    //! Jour
    uint8_t jour,
    //! Heure
    uint8_t heure,
    //! Minute
    uint8_t min,
    //! Seconde
    uint8_t sec) :
    THeureDistribution(nbMedic, NbSecondesSemaine(jour, heure, min, sec)) {
}

/**
 * @brief Retourne si cette heure de distribution est activée ou non en
 * utilisant le nombre de médicaments
 *
 * @return Si cette heure de distribution est activée
 * @retval true Cette heure de distribution est activée
 * @return false Cette heure de distribution n'est pas activée
 */
bool THeureDistribution::EstActif() const {
    return NbMedic > 0;
}

/**
 * @brief Convertie le nombre de secondes depuis le début de la semaine en
 * heure locale
 *
 * @return L'heure locale de cette distribution pour la semaine
 */
time_t THeureDistribution::HeureSemaine(const int position) const {
    // Calculer l'heure du début de cette semaine.
    const time_t heureLocale = Time.local();
    const uint32_t secSemaine = NbSecondesSemaine(heureLocale);
    const time_t debutSemaine = heureLocale - secSemaine;

    // Calculer l'heure de la distribution pour cette semaine.
    uint32_t resultat = debutSemaine + SecSemaine;

    // Adapter l'heure de distribution en fonction de la position de la
    // distribution.
    if (position == 0 || position == 1) { // précédente ou actuelle
        if (resultat > (uint32_t)heureLocale) {
            resultat -= NB_SECONDES_SEMAINE;
        }
    } else if (resultat < (uint32_t)heureLocale) { // suivante
        resultat += NB_SECONDES_SEMAINE;
    }

    // Calculer l'heure de cette distribution.
    return resultat;
}

/**
 * @brief Initialise un horaire de distribution vide.
 *
 */
THoraireDistribution::THoraireDistribution() : FTaille(0) {
}

/**
 * @brief Détermine la distribution précédente, la distribution en cours et
 * la distribution suivante en fonction de l'heure locale actuelle
 * @note La valeur nul peut être donnée à n'importe quel paramètre
 * de cette méthode si connaître la distribution n'est pas nécessaire
 *
 */
void THoraireDistribution::Distribution(
    //! Pointeur vers la prochaine distribution de médicaments
    const THeureDistribution **precedente,
    //! Pointeur vers la distribution de médicaments en cours
    const THeureDistribution **enCours,
    //! Pointeur vers la distribution de médicaments suivante
    const THeureDistribution **suivante) const {
    uint32_t secSemaine = THeureDistribution::NbSecondesSemaine();

    // Trouver la distribution suivante.
    size_t iSuivant = 0;
    const THeureDistribution *distActive = nullptr;
    const THeureDistribution *distSuivante = nullptr;
    while (iSuivant < FTaille && distSuivante == nullptr) {
        distActive = &FHoraire[iSuivant];
        if (distActive->SecSemaine > secSemaine) {
            distSuivante = distActive;
        } else {
            iSuivant++;
        }
    }
    if (distSuivante == nullptr && distActive != nullptr) {
        // La distribution suivante est celle qui est au début de la
        // semaine suivante, donc la première de la semaine.
        distSuivante = &FHoraire[0];
        iSuivant = 0;
    }

    if (distSuivante != nullptr) {
        if (suivante != nullptr) {
            *suivante = distSuivante;
        }

        if (enCours != nullptr) {
            // Trouver la distribution en cours.
            size_t iEnCours = iSuivant == 0 ? FTaille - 1 : iSuivant - 1;
            *enCours = &FHoraire[iEnCours];
        }

        if (precedente != nullptr) {
            // Trouver la distribution précédente.
            size_t iPrecedente;
            if (FTaille == 1) {
                iPrecedente = 0;
            } else if (FTaille == 2) {
                iPrecedente = iSuivant;
            } else if (iSuivant < 2) {
                iPrecedente = FTaille - (2 - iSuivant);
            } else {
                iPrecedente = iSuivant - 2;
            }
            *precedente = &FHoraire[iPrecedente];
        }
    } else {
        if (suivante != nullptr) {
            *suivante = nullptr;
        }
        if (enCours != nullptr) {
            *enCours = nullptr;
        }
        if (precedente != nullptr) {
            *precedente = nullptr;
        }
    }
}

/**
 * @brief Modifie l'horaire de distribution pour celui donné.
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Le pointeur nouvelHoraire est nul
 * @retval 2 Le nb de médicaments d'une des distributions est de 0
 * @retval 3 L'heure d'une des distributions à l'horaire est plus grande
 * que NB_DISTRIBUTIONS_SEMAINE
 * @retval 4 La différence entre l'heure de deux distributions adjacentes
 * est inférieure à DELAI_DISTRIBUTIONS_MIN
 */
int THoraireDistribution::Modifier(
    //! Nouvel horaire
    const THeureDistribution *nouvelHoraire,
    //! Nombre de distributions dans l'horaire ou -1 pour qu'il soit calculé
    int nbDist) {
    int codeErreur = 1;

    if (nouvelHoraire != nullptr || nbDist == 0) {
        if (nbDist < 0) {
            // Compter le nombre d'heures de distribution du nouvel horaire.
            nbDist = 0;
            while (!nouvelHoraire[nbDist].EstActif() &&
                   nbDist < NB_DISTRIBUTIONS_SEMAINE) {
                nbDist++;
            }
        }

        // Valider le nouvel horaire.
        codeErreur = 0;
        uint32_t heurePrecedente = 0 - DELAI_DISTRIBUTIONS_MIN;
        for (int i = 0; i < nbDist && codeErreur == 0; i++) {
            const THeureDistribution &heure = nouvelHoraire[i];
            if (!heure.EstActif()) {
                codeErreur = 2;
            } else if (heure.SecSemaine >= NB_SECONDES_SEMAINE) {
                codeErreur = 3;
            } else if (heure.SecSemaine <
                       (heurePrecedente + DELAI_DISTRIBUTIONS_MIN)) {
                codeErreur = 4;
            }

            heurePrecedente = heure.SecSemaine;
        }
        if (codeErreur == 0 && nbDist > 1) {
            const THeureDistribution &premier = nouvelHoraire[0];
            const THeureDistribution &dernier = nouvelHoraire[nbDist - 1];

            if (premier.SecSemaine +
                    (NB_SECONDES_SEMAINE - dernier.SecSemaine) <
                DELAI_DISTRIBUTIONS_MIN) {
                codeErreur = 4;
            }
        }

        // Enregistrer le nouvel horaire.
        if (codeErreur == 0) {
            if (nouvelHoraire != nullptr) {
                memcpy(FHoraire, nouvelHoraire,
                       sizeof(THeureDistribution) * nbDist);
            }

            if (nbDist < NB_DISTRIBUTIONS_SEMAINE) {
                FHoraire[nbDist].NbMedic = 0;
            }
            FTaille = nbDist;
        }
    }

    return codeErreur;
}

/**
 * @brief Retourne la liste d'heure de distribution.
 *
 * @return La liste d'heure de distribution.
 */
const THeureDistribution *THoraireDistribution::Distributions() const {
    return FHoraire;
}

/**
 * @brief Retourne le nombre de distributions
 *
 * @return Le nombre de distributions
 */
size_t THoraireDistribution::NbDistributions() const {
    return FTaille;
}
