/**
 * @file test.cpp
 * @brief Test la communication Bluetooth avec une application mobile
 */

#include <Particle.h>

// Ne pas attendre une connexion au réseau avant l'exécution de `void setup()`.
SYSTEM_THREAD(ENABLED);

// Ne pas se connecter automatiquement au réseau cellulaire.
SYSTEM_MODE(MANUAL);

//! Données à annoncer périodiquement par Bluetooth.
//! @see BLE.advertise
static BleAdvertisingData donneesAnnonce;
//! Données à retourner lors d'un scan Bluetooth.
//! @see BLE.advertise
static BleAdvertisingData donneesScan;

//! ID du service bluetooth personnalisé pour toutes
//! les caractéristiques qui suivent.
static const BleUuid
    servicePersonnalise("18944ab4-54a3-472a-871f-9b4dc69c5741");
//! Caractéristique de lecture d'une valeur.
//! permet au client Bluetooth de lire une valeur du distributeur.
static BleCharacteristic lireMsg("lire", BleCharacteristicProperty::READ,
                                 "0b450138-b45a-47ad-b3ef-ee16a4323007",
                                 servicePersonnalise);
//! Caractéristique de lecture d'une valeur.
//! permet au client Bluetooth de lire une valeur du distributeur.
static BleCharacteristic notifMsg("notif", BleCharacteristicProperty::NOTIFY,
                                  "7485df96-8867-41b6-8d85-c77cdc95e4bf",
                                  servicePersonnalise);

void receptionMsg(const uint8_t *donnees, size_t taille, const BlePeerDevice &,
                  void *);
//! Caractéristique de lecture d'une valeur.
//! permet au client Bluetooth de lire une valeur du distributeur.
static BleCharacteristic ecrireMsg("ecrire",
                                   BleCharacteristicProperty::WRITE_WO_RSP,
                                   "5f9b7668-d164-4d06-9163-4b189279d56e",
                                   servicePersonnalise, receptionMsg, NULL);

//! Si un appareil Bluetooth est connecté au contrôleur.
static volatile bool estConnecte = false;
//! Si un appareil était précédemment connecté au contrôleur.
static bool etaitConnecte = false;

//! Texte de la notification.
String notification;

/**
 * @brief Affiche les données reçues en tant que chaîne de caractères
 * sur le port sériel
 *
 */
void receptionMsg(
    //! Données reçues
    const uint8_t *donnees,
    //! Nombre d'octets reçus
    size_t taille, const BlePeerDevice &, void *) {
    Serial.printlnf("Message reçu: %.*s", taille, (const char *)donnees);
}

/**
 * @brief Active le sémaphore signalant qu'un appareil est connecté.
 * @note À passer en paramètre à BLE.onConnected
 *
 */
void appareilConnecte(const BlePeerDevice &, void *) {
    estConnecte = true;
}

/**
 * @brief Désactive le sémaphore signalant qu'un appareil est connecté.
 * @note À passer en paramètre à BLE.onDisconnected
 *
 */
void appareilDeconnecte(const BlePeerDevice &, void *) {
    estConnecte = false;
}

void setup() {
    Serial.begin(9600);

    BLE.on();

    BLE.addCharacteristic(lireMsg);
    BLE.addCharacteristic(notifMsg);
    BLE.addCharacteristic(ecrireMsg);

    BLE.onConnected(appareilConnecte, nullptr);
    BLE.onDisconnected(appareilDeconnecte, nullptr);

    donneesScan.appendLocalName("Medicator");
    donneesAnnonce.appendServiceUUID(servicePersonnalise);
    BLE.advertise(&donneesAnnonce, &donneesScan);
}

void loop() {
    // Afficher les connexions et les déconnexions.
    if (estConnecte != etaitConnecte) {
        etaitConnecte = estConnecte;
        Serial.println(estConnecte ? "Appareil connecté"
                                   : "Appareil déconnecté");
    }

    // Changer le message de notification.
    if (Serial.available()) {
        String s;
        int c;
        do {
            c = Serial.read();
            if (c > 0) {
                Serial.print((char)c);
                s += (char)c;
            }
        } while (c != '\n');
        s.trim();

        notification = s;
        notifMsg.setValue(notification);
        lireMsg.setValue(notification);

        Serial.printlnf("Notification envoyée");
    }
}
