/**
 * @file TGestionnaireEEPROM.cpp
 * @brief Définition de TGestionnaireDistribution
 */

#include "TGestionnaireEEPROM.h"
#include <crc32.h> // crc32_compute

//! Taille d'un CRC32 en octets.
static const TAdresseEEPROM TAILLE_CRC = 4;

//------------------------------------------------------------------------------
// Implémentation de TAllocationEEPROM
//------------------------------------------------------------------------------

/**
 * @brief Initialise une allocation vide
 *
 */
TAllocationEEPROM::TAllocationEEPROM() :
    FEeprom(nullptr), FAdresse(0), FTaille(0), FIntegre(false) {
}

/**
 * @brief Change l'allocation de cet objet et vérifie l'intégrité de ce nouvel
 * espace mémoire
 * @note Réservé à TGestionnaireEEPROM
 *
 * @return Un code d'erreur
 */
int TAllocationEEPROM::Allouer(
    //! EEPROM
    TAt25m02 &eeprom,
    //! Adresse du début de cet espace mémoire
    TAdresseEEPROM adresse,
    //! Taille de cet espace mémoire
    TAdresseEEPROM taille) {
    int codeErreur = 1;

    // Valider l'espace mémoire.
    if (taille >= TAILLE_CRC) {
        codeErreur = 0;

        FEeprom = &eeprom;
        FAdresse = adresse;
        FTaille = taille;

        // Vérifier l'intégrité de l'espace mémoire.
        taille -= TAILLE_CRC;
        uint8_t buf[taille];
        FEeprom->Lire(adresse, buf, taille);
        uint32_t crcLu = 0;
        FEeprom->Lire(adresse + taille, (uint8_t *)&crcLu, TAILLE_CRC);
        uint32_t crcCalcule = crc32_compute(buf, taille, nullptr);
        FIntegre = crcLu == crcCalcule;
    }

    return codeErreur;
}

/**
 * @brief Retourne si cet espace est alloué ou non
 *
 * @return Si cet espace est alloué ou non
 */
bool TAllocationEEPROM::EstAlloue() {
    return FEeprom != nullptr;
}

/**
 * @brief Retourne si cet espace est intègre ou non.
 * @note Cela signifie que les données qu'il contient correspondent au CRC, donc
 * elles devraient être valides.
 *
 * @return Si cet espace est intègre ou non
 */
bool TAllocationEEPROM::EstIntegre() {
    return FIntegre;
}

/**
 * @brief Écrire les données suivantes dans le EEPROM, recalcul le CRC et
 * l'écrit
 * @note Si cet espace mémoire n'est pas intègre, il est nécessaire de commencer
 * par appeler cet méthode une fois pour remplir cet espace mémoire avec des
 * données valides. Toute tentative d'écrire partiellement dans cet espace
 * mémoire alors qu'il n'est pas intègre va produire un code d'erreur `5` et ne
 * va rien écrire dans l'espace mémoire. Ceci est nécessaire, puisqu'il faut
 * remplir l'espace mémoire au complet une première fois avec des données
 * valides avant de pouvoir calculer son CRC32.
 * @see EstIntegre
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Cet espace n'est pas alloué
 * @retval 2 Le paramètre `donnees` est nul
 * @retval 3 Les données demandées dépassent cet espace mémoire
 * @retval 4 Cet espace mémoire n'est pas intègre
 * @retval 5 On tente d'écrire partiellement dans cet espace mémoire alors qu'il
 * n'est pas intègre
 */
int TAllocationEEPROM::Ecrire(
    //! Données à écrire
    const uint8_t *donnees,
    //! Taille du tampon de données à écrire
    TAdresseEEPROM taille,
    //! Décalage par rapport au début de cet espace mémoire à partir duquel
    //! écrire les données
    TAdresseEEPROM decalage) {
    int codeErreur = 0;

    if (FEeprom == nullptr) { // Si cet espace n'est pas alloué
        codeErreur = 1;
    } else if (donnees == nullptr) { // Si le paramètre `donnees` est nul
        codeErreur = 2;
    } else if (taille - decalage > Taille()) { // Si les données à écrire
                                               // dépassent cet espace mémoire
        codeErreur = 3;
    } else if (!FIntegre &&
               taille !=
                   Taille()) { // Si on tente d'écrire partiellement dans cet
                               // espace mémoire alors qu'il n'est pas intègre
        codeErreur = 4;
    } else {
        // Écrire les données.
        FEeprom->Ecrire(FAdresse + decalage, donnees, taille);

        // Mettre à jour le CRC.
        uint32_t crc32;
        if (decalage > 0) {
            uint8_t tampon[decalage];
            FEeprom->Lire(FAdresse, tampon, decalage);
            crc32 = crc32_compute(tampon, decalage, nullptr);
            crc32 = crc32_compute(donnees, taille, &crc32);
        } else {
            crc32 = crc32_compute(donnees, taille, nullptr);
        }

        TAdresseEEPROM tailleModifiee = decalage + taille;
        TAdresseEEPROM tailleRestant = Taille() - tailleModifiee;
        if (tailleRestant > 0) {
            uint8_t tampon[tailleRestant];
            FEeprom->Lire(FAdresse + tailleModifiee, tampon, decalage);
            crc32 = crc32_compute(tampon, tailleRestant, &crc32);
        }

        FEeprom->Ecrire(FAdresse + Taille(), (uint8_t *)&crc32, TAILLE_CRC);
        FIntegre = true;
    }

    return codeErreur;
}

/**
 * @brief Lire des données de cet espace mémoire si la mémoire n'est pas
 * corrompue
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Cet espace n'est pas alloué
 * @retval 2 Le paramètre `donnees` est nul
 * @retval 3 Les données demandées dépassent cet espace mémoire
 * @retval 4 Cet espace mémoire n'est pas intègre
 */
int TAllocationEEPROM::Lire(
    //! Tampon où écrire les données lues
    uint8_t *donnees,
    //! Nombre des données à lire
    TAdresseEEPROM taille,
    //! Décalage par rapport au début de cet espace mémoire à partir duquel
    //! lire les données
    TAdresseEEPROM decalage) {
    int codeErreur = 0;

    if (FEeprom == nullptr) { // Si cet espace n'est pas alloué
        codeErreur = 1;
    } else if (donnees == nullptr) { // Si le paramètres `donnees` est nul
        codeErreur = 2;
    } else if (taille + decalage > Taille()) { // Si les données demandées
                                               // dépassent cet espace mémoire
        codeErreur = 3;
    } else if (!FIntegre) { // Si l'espace mémoire n'est pas intègre.
        codeErreur = 4;
    } else {
        // Lire les données et les placer dans le tampon fourni.
        FEeprom->Lire(FAdresse + decalage, donnees, taille);
    }

    return codeErreur;
}

/**
 * @brief Retourne l'adresse du début de cet espace mémoire
 *
 * @return L'adresse du début de cet espace mémoire
 */
TAdresseEEPROM TAllocationEEPROM::Adresse() {
    return FAdresse;
}

/**
 * @brief Retourne la taille de cet espace mémoire (sans inclure le CRC)
 *
 * @return La taille de cet espace mémoire (sans inclure le CRC)
 */
TAdresseEEPROM TAllocationEEPROM::Taille() {
    return FTaille - TAILLE_CRC;
}

/**
 * @brief Retourne la taille de cet espace mémoire (en incluant le CRC)
 *
 * @return La taille de cet espace mémoire (en incluant le CRC)
 */
TAdresseEEPROM TAllocationEEPROM::TailleTotale() {
    return FTaille;
}

//------------------------------------------------------------------------------
// Implémentation de TGestionnaireEEPROM
//------------------------------------------------------------------------------

/**
 * @brief Initialise un nouveau gestionnaire de EEPROM pour le EEPROM donné
 *
 */
TGestionnaireEEPROM::TGestionnaireEEPROM(
    //! EEPROM où lire et écrire des données
    TAt25m02 &eeprom) :
    FEeprom(eeprom),
    FProchaineAllocation(0) {
    // Se débarrasser de l'avertissement causé par le fait que la variable
    // EEPROM inclue dans EEPROM.h n'est pas utilisée.
    (void)EEPROM;
}

/**
 * @brief Alloue un nouvel espace EEPROM à la prochaine adresse disponible
 *
 * @return Un code d'erreur
 * @retval -1 Il ne reste plus assez d'espace à allouer pour allouer un espace
 * de la taille donnée
 */
int TGestionnaireEEPROM::Allouer(
    //! Taille à allouer à cette espace mémoire (sans inclure le CRC)
    TAdresseEEPROM taille,
    //! Allocation à changer
    TAllocationEEPROM &allocation) {
    int codeErreur = -1;

    // Calculer la taille de la prochaine allocation.
    taille += TAILLE_CRC;

    // Valider qu'il reste assez d'espace pour une nouvelle allocation.
    if (TAt25m02::TAILLE_MEMOIRE - FProchaineAllocation >= taille) {
        codeErreur =
            allocation.Allouer(this->FEeprom, FProchaineAllocation, taille);

        // Si l'allocation a pu être effectuer, mettre à jour
        // l'adresse de la prochaine allocation.
        if (codeErreur == 0) {
            FProchaineAllocation += taille;
        }
    }

    return codeErreur;
}

/**
 * @brief Retourne l'espace libre (qui n'estpas encore alloué) dans le EEPROM
 *
 * @return L'espace libre (qui n'estpas encore alloué) dans le EEPROM
 */
TAdresseEEPROM TGestionnaireEEPROM::EspaceRestant() {
    return TAt25m02::TAILLE_MEMOIRE - FProchaineAllocation;
}

/**
 * @brief Retourne l'espace libre (qui n'estpas encore alloué) dans le EEPROM
 *
 * @return L'espace déjà alloué dans le EEPROM
 */
TAdresseEEPROM TGestionnaireEEPROM::EspaceAlloue() {
    return FProchaineAllocation;
}
