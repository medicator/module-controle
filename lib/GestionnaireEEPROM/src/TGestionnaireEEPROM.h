/**
 * @file TGestionnaireEEPROM.h
 * @brief Déclaration de TGestionnaireEEPROM, TAdresseEEPROM et
 * TAllocationEEPROM
 */

#ifndef TGestionnaireEEPROM_H
#define TGestionnaireEEPROM_H

#include <TAt25m02.h>
#include <stdint.h>

//! Représente une adresse dans le EEPROM
typedef uint16_t TAdresseEEPROM;

class TGestionnaireEEPROM;

//! Représente un espace mémoire alloué dans le EEPROM
//! dont l'intégrité est assurée par un CRC32
class TAllocationEEPROM {
private:
    //! EEPROM
    TAt25m02 *FEeprom;

    //! Adresse du début de cet espace mémoire
    TAdresseEEPROM FAdresse;
    //! Taille de cet espace mémoire (incluant le CRC)
    TAdresseEEPROM FTaille;

    //! Si cet espace mémoire est intègre ou corrompu
    bool FIntegre;

    //! Permet de donner accès à #Allouer à TGestionnaireEEPROM
    friend class TGestionnaireEEPROM;

    int Allouer(TAt25m02 &eeprom, TAdresseEEPROM adresse,
                TAdresseEEPROM taille);

public:
    TAllocationEEPROM();

    bool EstAlloue();
    bool EstIntegre();

    int Ecrire(const uint8_t *donnees, TAdresseEEPROM taille,
               TAdresseEEPROM decalage = 0);
    /**
     * @brief Écrire les données suivantes dans le EEPROM, recalcul le CRC et
     * l'écrit
     *
     * @return Un code d'erreur
     * @retval 0 Aucune erreur
     * @retval 1 Cet espace n'est pas alloué
     * @retval 2 Le paramètre `donnees` est nul
     * @retval 3 des données devraient être écrites en dehors de cet espace
     * mémoire
     */
    template <typename T>
    int Ecrire(
        //! Données à écrire
        const T &donnees,
        //! Décalage par rapport au début de cet espace mémoire à partir duquel
        //! écrire les données
        TAdresseEEPROM decalage = 0) {
        return Ecrire((const uint8_t *)&donnees, sizeof(T), decalage);
    }

    int Lire(uint8_t *donnees, TAdresseEEPROM taille,
             TAdresseEEPROM decalage = 0);
    /**
     * @brief Lit des données de cet espace mémoire si la mémoire n'est pas
     * corrompue
     *
     * @tparam T Type des données à lire du EEPROM
     * @return Un code d'erreur
     * @retval 0 Aucune erreur
     * @retval 1 Cet espace n'est pas alloué
     * @retval 2 Le paramètre `donnees` est nul
     * @retval 3 Les données demandées dépassent cet espace mémoire
     * @retval 4 Cet espace mémoire n'est pas intègre
     */
    template <typename T>
    int Lire(
        //! Tampon où écrire les données lues
        T &donnees,
        //! Décalage par rapport au début de cet espace mémoire à partir duquel
        //! lire les données
        TAdresseEEPROM decalage = 0) {
        return Lire((uint8_t *)&donnees, sizeof(T), decalage);
    }

    TAdresseEEPROM Adresse();
    TAdresseEEPROM Taille();
    TAdresseEEPROM TailleTotale();
};

//! Gère l'accès de plusieurs objets à différentes
//! partie du EEPROM et permet de facilement s'assurer
//! de l'intégrité de chacune de ces partie
class TGestionnaireEEPROM {
private:
    //! EEPROM
    TAt25m02 &FEeprom;

    //! Adresse de la prochaine allocation à effectuer dans le EEPROM
    TAdresseEEPROM FProchaineAllocation;

public:
    TGestionnaireEEPROM(TAt25m02 &eeprom);

    int Allouer(TAdresseEEPROM taille, TAllocationEEPROM &allocation);

    TAdresseEEPROM EspaceRestant();
    TAdresseEEPROM EspaceAlloue();
};

#endif /* TGestionnaireEEPROM_H */
