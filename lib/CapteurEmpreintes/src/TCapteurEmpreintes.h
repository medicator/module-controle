/**
 * @file CapteurEmpreintes.h
 * @brief Déclaration de TCapteurEmpreintes
 */

#ifndef CapteurEmpreintes_H
#define CapteurEmpreintes_H

#include <Adafruit_Fingerprint.h>
#include <stdint.h>

//! Facilite l'utilisation du capteur d'empreintes digitales
class TCapteurEmpreintes : public Adafruit_Fingerprint {
public:
    //! Type de la fonction passée en paramètre à EnregistrerEmpreinte et
    //! AuthentifierEmpreinte qui est appelée pour signaler que l'utilisateur
    //! doit placer ou enlever son doigt du capteur.
    //!
    //! Lorsque le paramètre enlever est à true, l'utilisateur doit enlever son
    //! doigt, sinon il doit le placer sur le capteur.
    typedef void (*TDeplacerDoigt)(bool enlever);

    //! Temps mort par défaut en millisecondespour l'enrollement et
    //! l'authentification d'une empreinte. Cette valeur est de 10s.
    static const uint32_t TEMPS_MORT_DEFAUT = 10 * 1000;

    // Importer le constructeur.
    using Adafruit_Fingerprint::Adafruit_Fingerprint;

    // Importer la méthode begin.
    using Adafruit_Fingerprint::begin;
    // Définir une seconde méthode begin.
    void begin();

    int EnregistrerEmpreinte(uint8_t doigt,
                             TDeplacerDoigt deplacerDoigt = nullptr,
                             uint32_t tempsMort = TEMPS_MORT_DEFAUT);
    int AuthentifierEmpreinte(TDeplacerDoigt deplacerDoigt = nullptr,
                              uint32_t tempsMort = TEMPS_MORT_DEFAUT);
    int AuthentifierEmpreinteRapidement();

    int EffacerEmpreinte(uint8_t doigt);
    int ReinitialiserEmpreintes();

private:
    int EnregistrerGabarit(bool fente, TDeplacerDoigt deplacerDoigt,
                           uint32_t tempsMort);
};

#endif /* CapteurEmpreintes_H */
