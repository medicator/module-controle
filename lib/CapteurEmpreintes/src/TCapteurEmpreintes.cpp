/**
 * @file CapteurEmpreintes.h
 * @brief Implémentation de TCapteurEmpreintes
 */

#include <Arduino.h>
#include <TCapteurEmpreintes.h>

//! Vitesse de communication avec le capteur d'empreintes digitales
#define VITESSE_COMM_DEFAUT 57600

/**
 * @brief Initialise la communication avec le capteur d'empreintes digitales à
 * 57600 baud
 * @note Cette méthode ou begin(uint16_t) doit être appelée avant n'importe
 * quelle autre méthode après le constructeur
 */
void TCapteurEmpreintes::begin() {
    begin(VITESSE_COMM_DEFAUT);
}

/**
 * @brief Lit une empreinte, la converti en gabarit et la place dans la fente
 * donnée
 *
 * @return int Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1-21 @see FINGERPRINT_*
 */
int TCapteurEmpreintes::EnregistrerGabarit(
    //! Fente ou placer le gabarit. false pour 1, true pour 2.
    bool fente,
    //! Fonction à appeler pour signaler à l'utilisateur de placer ou d'enlever
    //! son doigt du capteur.
    TDeplacerDoigt deplacerDoigt,
    //! Temps mort après lequel cette fonction doit retourné
    uint32_t tempsMort) {
    // Début du décompte du temps mort.
    uint32_t debut = millis();

    // Signaler à l'utilisateur de placer son doigt sur le capteur.
    if (deplacerDoigt != nullptr) {
        deplacerDoigt(false);
    }

    // Déterminer la fente ou placer le gabarit.
    int iFente = fente ? 2 : 1;

    // Répéter tant que le gabarit n'a pas été créé et que le tant mort n'a pas
    // été atteint et qu'il n'y a pas eu d'erreurs de communication.
    int erreur;
    do {
        // Obtenir l'empreinte digitale.
        erreur = getImage();
        if (erreur == FINGERPRINT_OK) {
            // Convertir l'empreinte en gabarit.
            erreur = image2Tz(iFente);
        }
    } while (erreur != FINGERPRINT_PACKETRECIEVEERR &&
             erreur != FINGERPRINT_OK && (millis() - debut) < tempsMort);

    // Retourner le code d'erreur.
    return erreur;
}

/**
 * @brief Enregistre une nouvelle empreinte pour le doigt donné.
 *
 * @return int Un code d'erreur
 * @retval 0 Aucune erreur, l'empreinte a été enregistrée
 * @retval -1 L'index du doigt est invalide
 * @retval -2 L'utilisateur devait enlever son doigt, mais ne l'a pas fait
 * @retval 1-21 @see FINGERPRINT_*
 */
int TCapteurEmpreintes::EnregistrerEmpreinte(
    //! Index du doigt à enroller entre 0 et 9
    uint8_t doigt,
    //! Fonction appelée pour signaler que l'utilisateur doit placer ou enlever
    //! son doigt du capteur.
    TDeplacerDoigt deplacerDoigt,
    //! Temps mort pour chaque étape de l'enrollement d'une empreinte
    uint32_t tempsMort) {
    // Valider les paramètres.
    if (doigt > 9) {
        // Retourner le code de doigt invailde.
        return -1;
    }

    // Obtenir l'empreinte digitale une première fois et la convertir en
    // gabarit.
    int codeErreur = EnregistrerGabarit(false, deplacerDoigt, tempsMort);

    if (codeErreur == FINGERPRINT_OK) {
        // Attendre que l'utilisateur enlève son doigt.
        if (deplacerDoigt != nullptr) {
            deplacerDoigt(true);
        }

        // Tant que l'utilisateur n'a pas enlevé son doigt.
        /* int debut = millis();
        do {
            codeErreur = getImage();
            Serial.println(codeErreur);
        } while (codeErreur != FINGERPRINT_NOFINGER &&
                 (millis() - debut) < tempsMort);
        if (codeErreur != FINGERPRINT_NOFINGER) {
            // Retourner le code d'erreur signifiant que l'utilisateur n'a
        jamais
            // enlever son doigt.
            return -2;
        } */ // Cette section ne fonctionne pas voir l'issue #1.
             // Elle est remplacée par le délai ci-dessous.
        delay(2000);

        // Obtenir la même empreinte digitale une deuxième fois.
        codeErreur = EnregistrerGabarit(true, deplacerDoigt, tempsMort);
    }

    if (codeErreur == FINGERPRINT_OK) {
        // Créer un modèle à partir des empreintes.
        codeErreur = createModel();
        if (codeErreur != FINGERPRINT_OK) {
            return codeErreur;
        }
    }

    if (codeErreur == FINGERPRINT_OK) {
        // Enregistrer le modèle.
        codeErreur = storeModel(doigt);
    }

    return codeErreur;
}

/**
 * @brief Authentifier une empreinte en faisant plusieurs essais jusqu'à ce que
 * le temps mort soit atteint. Le résultat de l'authentification est disponible
 * dans la variable fingerId, qui est l'identifiant du doit authentifié, et
 * dans la variable confidence qui représente la confiance qu'à le capteur que
 * l'empreinte correspond au fingerId.
 *
 * @return int Un code d'erreur
 * @retval 0 Aucune erreur, l'empreinte a été enregistrée
 * @retval 1-21 @see FINGERPRINT_*
 */
int TCapteurEmpreintes::AuthentifierEmpreinte(
    //! Fonction appelée pour signaler que l'utilisateur doit placer ou enlever
    //! son doigt du capteur.
    TDeplacerDoigt deplacerDoigt,
    //! Temps mort pour chaque étape de l'enrollement d'une empreinte
    uint32_t tempsMort) {
    // Obtenir l'empreinte digitale et la convertir en gabarit.
    int codeErreur = EnregistrerGabarit(false, deplacerDoigt, tempsMort);

    // Rechercher l'empreinte digitale.
    if (codeErreur == FINGERPRINT_OK) {
        codeErreur = fingerFastSearch();
    }

    // Retourner le code d'erreur.
    return codeErreur;
}

/**
 * @brief Authentifier une empreinte sans faire plusieurs essais. Le résultat de
 * l'authentification est disponible dans la variable fingerId, qui est
 * l'identifiant du doit authentifié, et dans la variable confidence qui
 * représente la confiance qu'à le capteur que l'empreinte correspond au
 * fingerId.
 *
 * @return int Un code d'erreur
 * @retval 0 Aucune erreur, l'empreinte a été enregistrée
 * @retval -1 L'index du doigt est invalide
 * @retval 1-21 @see FINGERPRINT_*
 */
int TCapteurEmpreintes::AuthentifierEmpreinteRapidement() {
    // Obtenir l'empreinte.
    int codeErreur = getImage();

    // Convertir l'empreinte en gabarit.
    if (codeErreur == FINGERPRINT_OK) {
        codeErreur = image2Tz();
    }

    // Trouver une correspondance.
    if (codeErreur == FINGERPRINT_OK) {
        codeErreur = fingerFastSearch();
    }

    // Retourner le code d'erreur.
    return codeErreur;
}

/**
 * @brief Efface l'empreinte enregistrée pour le doigt donné
 *
 * @return int Un code d'erreur
 * @retval 0 Aucune erreur, l'empreinte a été enregistrée
 * @retval -1 l'index du doigt est invalide
 * @retval 1-21 @see FINGERPRINT_*
 */
int TCapteurEmpreintes::EffacerEmpreinte(
    //! Index du doigt à enroller entre 0 et 9
    uint8_t doigt) {
    // Valider les paramètres.
    if (doigt > 9) {
        return -1;
    }

    // Effacer l'empreinte de la mémoire.
    return deleteModel(doigt);
}

/**
 * @brief Réinitialise la liste de toutes les empreintes enregistrées
 *
 * @return int Un code d'erreur
 * @retval 0 Aucune erreur, l'empreinte a été enregistrée
 * @retval 1-21 @see FINGERPRINT_*
 */
int TCapteurEmpreintes::ReinitialiserEmpreintes() {
    // Supprimer toutes les empreintes.
    return emptyDatabase();
}
