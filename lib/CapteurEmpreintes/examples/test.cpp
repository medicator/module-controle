/**
 * @file test.cpp
 * @brief Test de l'enregistrement et de l'authentification d'empreintes
 * digitales
 */

#include <Particle.h>
#include <TCapteurEmpreintes.h>

#include <Arduino.h>

// Ne pas attendre une connexion au réseau avant l'exécution de `void setup()`.
SYSTEM_THREAD(ENABLED);

// Ne pas se connecter automatiquement au réseau cellulaire.
SYSTEM_MODE(MANUAL);

//! Capteur d'empreintes digitales.
TCapteurEmpreintes capteur(&Serial1);

void avertirUtilisateur(bool enlever) {
    Serial.println(enlever ? "Enlever votre doigt" : "Placer votre doigt");
}

void setup() {
    // Initialisation du port sériel.
    Serial.begin(9600);

    // Initialiser le capteur d'empreintes.
    capteur.begin();

    Serial.println("Menu");
    Serial.println("  [1,10]: Index du doigt que vous voulez enregistrer");
    Serial.println("[-1,-10]: Index du doigt que vous voulez effacer");
    Serial.println("       0: Réinitialiser toutes les empreintes");
}

void loop() {
    if (Serial.available()) {
        // Obtenir l'index du doigt à authentifier
        int doigt = Serial.readStringUntil('\n').toInt();

        int erreur;
        if (doigt > 0) {
            // Enroller l'empreinte.
            Serial.print("Configuration de l'empreinte #");
            Serial.println(doigt);
            erreur =
                capteur.EnregistrerEmpreinte(doigt - 1, avertirUtilisateur);
        } else if (doigt == 0) {
            // Réinitialiser toutes les empreintes.
            Serial.println("Réinitialisation de toutes les empreintes");
            erreur = capteur.ReinitialiserEmpreintes();
        } else { // if (doigt < 0)
            // Supprimer une empreinte.
            doigt += 10;
            Serial.print("Suppression de l'empreinte #");
            Serial.println(doigt);
            erreur = capteur.EffacerEmpreinte(doigt);
        }

        // Afficher le code d'erreur.
        if (erreur == 0) {
            Serial.println("Succès!");
        } else {
            Serial.printlnf("Erreur: %i", erreur);
        }

        Serial.println();
        while (Serial.available() != 0) {
            Serial.read();
        }
    }

    int erreur = capteur.AuthentifierEmpreinteRapidement();
    if (erreur != FINGERPRINT_NOFINGER) {
        if (erreur != 0) {
            Serial.print("Erreur d'authentification d'empreinte: ");
            Serial.println(erreur);
        } else {
            Serial.print("Empreinte #");
            Serial.print(capteur.fingerID + 1);
            Serial.print(" détectée avec une confiance de ");
            Serial.println(capteur.confidence);
        }
        Serial.println();
    }
}
