/**
 * @file test.cpp
 * @brief Programme de test des classes TDel, TDelCellulaire et
 * TDelClignotteTemp.
 */

#include <Arduino.h>

#include <TDel.h>
#include <TDelCellulaire.h>
#include <TDelClignotteTemp.h>

// Ne pas attendre une connexion au réseau avant l'exécution de `void setup()`.
SYSTEM_THREAD(ENABLED);

// Ne pas se connecter automatiquement au réseau cellulaire.
SYSTEM_MODE(MANUAL);

//! DEL pour signaler l'état du Bluetooth.
TRefDel<TDel> delBluetooth(D6);

//! DEL pour afficher l'état du réseau.
TRefDel<TDelCellulaire> delCellulaire(D7);

//! DEL pour signaler une erreur.
TRefDel<TDelClignotteTemp> delErreur(D8);

void setup() {
    Serial.begin(9600);

    delBluetooth.Del().Etat(edClignotte);
    TGestionnaireDels::begin(1000);

    Cellular.on();
}

void loop() {
    Serial.print('.');
    delay(100);

    if (Serial.available()) {
        Serial.read();
        delErreur.Del().FaireClignotter();
    }
}
