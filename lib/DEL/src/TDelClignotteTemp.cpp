/**
 * @file TDelClignotteTemp.cpp
 * @brief Implémentation de TDelClignotteTemp
 */

#include "TDelClignotteTemp.h"

/**
 * @brief Initialise une DEL à clignottement temporaire avec la broche donnée et
 * le nombre de clignottements par défaut.
 *
 */
TDelClignotteTemp::TDelClignotteTemp(
    //! Broche de la DEL
    int broche) :
    TDel(broche),
    FNbClignottements(NB_CLIGNOTTEMENTS_DEFAUT), FNbClignottementsRestants(0) {
}

/**
 * @brief Fait clignotter la DEL pour le nombre de clignottement configuré ou le
 * nombre de clignottement par défaut si aucun n'a été configuré
 * @see NB_CLIGNOTTEMENTS_DEFAUT
 *
 */
void TDelClignotteTemp::FaireClignotter() {
    // Assigner le double du nombre de clignottements au nombre de
    // clignottements restants.
    FNbClignottementsRestants = FNbClignottements * 2;
}

/**
 * @brief Retourne si la DEL est actuellement en train de clignotter ou non
 *
 * @return Si la DEL est en train de clignotter
 * @retval true La DEL est en train de clignotter
 * @return false La DEL n'est pas en train de clignotter. Elle est éteinte.
 */
bool TDelClignotteTemp::Clignotte() {
    return FNbClignottementsRestants > 0;
}

/**
 * @brief Change le nombre de clignottement
 * @note Ceci n'affectera pas le clignottement en cours
 *
 */
void TDelClignotteTemp::NbClignottements(
    //! Nouveau nombre de clignottements
    uint8_t nbClignottements) {
}

/**
 * @brief Retourne le nombre de clignottements configuré
 *
 * @return uint8_t Le nombre de clignottements configuré
 */
uint8_t TDelClignotteTemp::NbClignottements() {
    return FNbClignottements;
}

/**
 * @brief DEL qui est allumé lorsque le contrôleur est connecté au réseau et
 * clignotte lorsque ce n'est pas le cas.
 */
void TDelClignotteTemp::MaJEtat() {
    TEtatDel etat = edEteinte;

    // S'il reste des clignottements à effectuer
    if (FNbClignottementsRestants > 0) {
        // Faire clignotter la DEL.
        etat = edClignotte;

        // Décrémenter le nombre de clignottements restants.
        FNbClignottementsRestants--;
    }

    Etat(etat);
}
