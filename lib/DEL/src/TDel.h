/**
 * @file TDel.h
 * @brief Déclaration de TDel et TEtatDel
 */

#ifndef TDel_H
#define TDel_H

#include <Particle.h> // Timer
#include <stdint.h>   // uint8_t, uint32_t

//! Représente l'état d'une DEL pour TDel
enum TEtatDel {
    //! La DEL est allumée
    edAllumee,
    //! La DEL est éteinte
    edEteinte,
    //! La DEL clignotte
    edClignotte,
    //! La DEL est inactive. Son état ne sera pas modifié par TDel lors de la
    //! prochaine interruption. Utiliser ceci lorsque vous voulez avoir le
    //! contrôle total sur la DEL pendant une période donnée.
    edInactive,

    edPremier = edAllumee,
    edDernier = edInactive,
};

//! Permet d'allumer d'éteindre et de faire clignotter des
//! DELs en utilisant des timers.
//! @note Cette classe ne peut pas directement être instanciée. Pour l'utiliser,
//! vous devez créer une TRefDel<TDel>. Pour avoir accès à la DEL vous pourrez
//! utiliser la méthode TRefDel<TDel>::Del.
class TDel {
    friend class TGestionnaireDels;
    template <class TTypeDel> friend class TRefDel;

private:
    //! Broche de la DEL
    const int FBroche;
    //! État de la DEL
    TEtatDel FEtat;

    //! Nombre de TRefDel possédant une référence de cette DEL.
    //! Lorsque ce nombre atteint 0, la DEL devrait être détruite.
    uint8_t FNbReferences;

    //! DEL précédente dans la liste chaînée.
    TDel *FDelPrecedente;
    //! DEL suivante dans la list chaînée.
    TDel *FDelSuivante;

    // Supprimer le constructeur de copie.
    TDel(TDel &del) = delete;

public:
    // Retire une DEL de la liste.
    virtual ~TDel();

    // Change l'état de la DEL
    int Etat(TEtatDel etat);
    // Retourne l'état de la DEL
    TEtatDel Etat();

protected:
    TDel(int broche);

    virtual void MaJEtat();
};

//! @brief Réference vers une TDel
//! @note Ceci est un pointeur à compte de référence d'objets de classe TDel
//!
//! @tparam TTypeDel Type de la DEL, ceci doit être un type qui hérite de TDel
//! @see TDel
template <class TTypeDel> class TRefDel {
private:
    //! Référence de la DEL.
    TTypeDel *FDel;

public:
    /**
     * @brief Créer une nouvelle référence de DEL d'une nouvelle DEL avec la
     * broche donnée.
     * @note La DEL initialisée est éteinte.
     *
     */
    TRefDel(
        //! Broche de la DEL
        int broche) :
        FDel(new TTypeDel(broche)) {
    }

    /**
     * @brief Copie une référence de DELs. La nouvelle référence pointera vers
     * la même DEL que l'ancienne.
     *
     */
    TRefDel(
        //! Référence de la DEL
        TRefDel<TTypeDel> &ref) :
        FDel(ref.FDel) {
        // Ajouter une référence à la DEL.
        FDel->FNbReferences++;
    }

    /**
     * @brief Supprime cette référence de la DEL et supprime la DEL si elle n'a
     * plus de références.
     */
    ~TRefDel() {
        // Décrémenter le nombre de références de cette DEL.
        FDel->FNbReferences--;

        // Si la DEL n'a plus de références
        if (FDel->FNbReferences == 0) {
            // Supprimer la DEL.
            delete FDel;
        }
    }

    /**
     * @brief Retourne la DEL vers laquelle pointe cette référence.
     *
     * @return TTypeDel& Une référence de la DEL
     */
    inline TTypeDel &Del() {
        return *FDel;
    }
};

//! Gère l'état d'une liste de DEL en utilisant
//! un Timer pour les faire clignotter
class TGestionnaireDels {
    // Supprimer le constructeur
    TGestionnaireDels() = delete;

    // Donner l'accès à toutes les propriétés de cette classe à TDel.
    friend class TDel;

private:
    //! Dernière DEL de la liste de DELs instanciées
    static TDel *FDerniereDel;

    //! Timer pour générer des interruptions périodiquement.
    static Timer FTimer;

    //! Permet de savoir si les DEL qui clignottent doivent être allumées ou
    //! fermées.
    static bool FClignottement;

    static void ISR();

public:
    static void begin(uint32_t periode);
};

#endif // TDel_H
