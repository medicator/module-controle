/**
 * @file TDelCellulaire.cpp
 * @brief Implémentation de TDelCellulaire
 */

#include "TDelCellulaire.h"

/**
 * @brief DEL qui est allumé lorsque le contrôleur est connecté au réseau et est
 * éteinte lorsqu'il en est déconnecté.
 */
void TDelCellulaire::MaJEtat() {
    Etat(Cellular.ready() ? edAllumee : edClignotte);
}
