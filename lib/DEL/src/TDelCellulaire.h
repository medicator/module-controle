/**
 * @file TDelCellulaire.h
 * @brief Déclaration de TDelCellulaire
 */

#ifndef TDelCellulaire_H
#define TDelCellulaire_H

#include "TDel.h"

//! DEL qui est allumé lorsque le contrôleur est connecté au réseau et est
//! éteinte lorsqu'il en est déconnecté.
class TDelCellulaire : private TDel {
    friend class TRefDel<TDelCellulaire>;

private:
    // Utiliser le constructeur de TDel.
    using TDel::TDel;

    // Supprimer le constructeur de copies.
    TDelCellulaire(TDel &del) = delete;

public:
protected:
    void MaJEtat() override;
};

#endif // TDelCellulaire_H
