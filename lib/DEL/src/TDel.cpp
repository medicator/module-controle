/**
 * @file TDel.cpp
 * @brief Implémentation de TDel, TGestionnaireDels
 */

#include "TDel.h"

//! Niveau logique pour allumer une DEL.
#define NIVEAU_DEL_ALLUMER 0
//! Niveau logique pour éteindre une DEL.
#define NIVEAU_DEL_ETEINDRE 1

/**
 * @brief Initialise une DEL éteinte avec la broche donnée et l'ajoute au
 * gestionnaire de DELs
 *
 */
TDel::TDel(
    //! Broche de la DEL
    int broche) :
    FBroche(broche),
    FNbReferences(1), FDelPrecedente(nullptr), FDelSuivante(nullptr) {
    // Configurer la broche en sortie.
    pinMode(FBroche, OUTPUT);

    // Éteindre la DEL.
    Etat(edEteinte);

    // Si cette DEL n'est pas la première.
    if (TGestionnaireDels::FDerniereDel != nullptr) {
        // Associer la DEL précédente à cette nouvelle DEL.
        FDelPrecedente = TGestionnaireDels::FDerniereDel;
        FDelPrecedente->FDelSuivante = this;
    }

    // Changer la dernière DEL du gestionnaire de DELs pour celle-ci.
    TGestionnaireDels::FDerniereDel = this;
}

/**
 * @brief Supprime une DEL de la liste de DELs de TGestionnaireDels en
 * supprimant toutes ses références de TGestionnaireDels::FDerniereDel et des
 * propriétés FDelPrecedente->FDelSuivante et FDelSuivante->FDelPrecedente.
 * Cette méthode supprime va également refaire la chaîne de manière à ce qu'elle
 * fonctionne encore par la suite.
 *
 */
TDel::~TDel() {
    // Si cette DEL est la dernière
    if (FDelSuivante == nullptr) {
        // S'il y a une DEL précédente.
        if (FDelPrecedente != nullptr) {
            // Supprimer cette DEL de la DEL précédente.
            FDelPrecedente->FDelSuivante = nullptr;

            // Remplacer la dernière DEL du gestionnaire de DEL par la DEL
            // précédente.
            TGestionnaireDels::FDerniereDel = FDelPrecedente;
        }
        // Sinon, cela signifie que cette DEL était unique
        else {
            // Supprimer la dernière DEL du gestionnaire de DELs.
            TGestionnaireDels::FDerniereDel = nullptr;
        }
    }
    // Sinon si une autre DEL suit cette DEL
    else {
        // S'il y a une DEL précédente.
        if (FDelPrecedente != nullptr) {
            // Lier la DEL suivante et la DEL précédente ensemble.
            // Par la même occasion les deux DELs vont perdre leur lien avec
            // celle-ci.
            FDelPrecedente->FDelSuivante = FDelSuivante;
            FDelSuivante->FDelPrecedente = FDelPrecedente;
        }
        // Sinon, cette DEL est la première
        else {
            // Supprimer le lien de la DEL suivante à cette DEL.
            FDelSuivante->FDelPrecedente = nullptr;
        }
    }
}

/**
 * @brief Change l'état de la DEL
 *
 * @return int un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 L'état donné est invalide
 */
int TDel::Etat(
    //! Nouvel état
    TEtatDel etat) {
    int codeErreur = 1;

    if (etat >= edPremier && etat <= edDernier) {
        if (etat == edAllumee) {
            digitalWrite(FBroche, NIVEAU_DEL_ALLUMER);
        } else if (etat == edEteinte) {
            digitalWrite(FBroche, NIVEAU_DEL_ETEINDRE);
        }

        FEtat = etat;
        codeErreur = 0;
    }

    return codeErreur;
}

/**
 * @brief Retourne l'état de la DEL
 *
 * @return TEtatDel l'état de la DEL
 */
TEtatDel TDel::Etat() {
    return FEtat;
}

/**
 * @brief Méthode appelée dans l'ISR pour permettre à la DEL de mettre à jour
 * son état. Ceci permet de créer des états supplémentaires.
 * @note Cette implémentation par défaut est vide.
 */
void TDel::MaJEtat() {
}

// Initialiser les variables statiques de TGestionnaireDels.
TDel *TGestionnaireDels::FDerniereDel;
Timer TGestionnaireDels::FTimer(1000, TGestionnaireDels::ISR);
bool TGestionnaireDels::FClignottement = true;

/**
 * @brief
 *
 */
void TGestionnaireDels::ISR() {
    // Pour chaque DEL dans le sens inverse
    TDel *del = FDerniereDel;
    while (del != nullptr) {
        // Permettre à la DEL de mettre à jour son état.
        del->MaJEtat();

        // Faire clignotter la DEL si nécessaire.
        if (del->FEtat == edClignotte) {
            digitalWrite(del->FBroche, FClignottement ? NIVEAU_DEL_ALLUMER
                                                      : NIVEAU_DEL_ETEINDRE);
        }

        // Passer à la prochaine del.
        del = del->FDelPrecedente;
    }

    // Faire en sorte que la prochaine fois que cette méthode soit appelée les
    // DELs qui doivent clignotter change d'état.
    FClignottement = !FClignottement;
}

/**
 * @brief Initialise le timer et le démarre
 * @note Cette méthode peut être rappelée plus tard pour changer la valeur de la
 * période, mais il ne faut pas l'appeler trop souvent sinon cela va perturber
 * les délais.
 */
void TGestionnaireDels::begin(
    //! Délai du timer global.
    uint32_t periode) {
    // Changer le période du timer pour celle donnée.
    FTimer.changePeriod(periode);

    // Démarrer le timer.
    FTimer.start();
}
