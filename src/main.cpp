/**
 * @file main.cpp
 * @brief Programme principal du module de contrôle
 */

#include <Arduino.h>
#include <Menu.h>
#include <TAt25m02.h>
#include <TBuzzer.h>
#include <TCapteurEmpreintes.h>
#include <TCommunication.h>
#include <TConnexionCellulaire.h>
#include <TDel.h>
#include <TDelCellulaire.h>
#include <TGestionnaireEEPROM.h>
#include <TModuleDistribution.h>

// Ne pas attendre une connexion au réseau avant l'exécution de `void setup()`.
SYSTEM_THREAD(ENABLED);

// Ne pas se connecter automatiquement au réseau cellulaire.
SYSTEM_MODE(MANUAL);

//! S'il faut ou non activer la connexion au réseau cellulaire.
#define ACTIVER_CONN_CELLULAIRE 0

//! EEPROM.
TAt25m02 eeprom(SPI, A5);
//! Gestionnaire du EEPROM.
TGestionnaireEEPROM gestionnaireEEPROM(eeprom);

//! DEL pour signaler qu'il y a un médicament à distribuer.
TRefDel<TDel> delMedicDispo(D6);
//! DEL pour afficher l'état du réseau.
TRefDel<TDelCellulaire> delCellulaire(D7);
//! DEL pour signaler une erreur.
TRefDel<TDelClignotteTemp> delErreur(D8);

//! Module de distribution.
TModuleDistribution moduleDistribution(delMedicDispo, delErreur);

//! Gestionnaire de communication.
TCommunication &communication =
    TCommunication::Instance(Wire, 9, moduleDistribution);

//! Capteur d'empreintes digitales.
TCapteurEmpreintes capteur(&Serial1);

#if ACTIVER_CONN_CELLULAIRE
//! Connexion au réseau cellulaire.
TConnexionCellulaire connCell(delErreur, 60 * 60 * 24);
#endif

//! Items du menu.
static const TItemMenu ITEMS_MENU[] = {
    TItemMenu("Afficher le nb de médicaments dans le réservoir",
              []() { MenuAfficherNbMedic(moduleDistribution); }),
    TItemMenu("Changer le nb de médicaments dans le réservoir",
              []() { MenuChangerNbMedic(moduleDistribution); }),
    TItemMenu("Afficher le nb de médicaments critique",
              []() { MenuAfficherNbMedicCritique(moduleDistribution); }),
    TItemMenu("Changer le nb de médicaments critique",
              []() { MenuChangerNbMedicCritique(moduleDistribution); }),
    TItemMenu("Afficher le délai de retard en secondes",
              []() { MenuAfficherDelaiRetard(moduleDistribution); }),
    TItemMenu("Changer le délai de retard en secondes",
              []() { MenuChangerDelaiRetard(moduleDistribution); }),
    TItemMenu("Afficher le nom du médicament",
              []() { MenuAfficherNomMedic(moduleDistribution); }),
    TItemMenu("Changer le nom du médicament",
              []() { MenuChangerNomMedic(moduleDistribution); }),
    TItemMenu("Déverrouiller le module de distribution",
              []() { MenuDeverrouiller(moduleDistribution); }),
    TItemMenu("Enregistrer une empreinte",
              []() { MenuEnregistrerEmpreinte(capteur); }),
    TItemMenu("Supprimer une empreinte",
              []() { MenuSupprimerEmpreinte(capteur); }),
    TItemMenu("Afficher la liste de contacts",
              []() { MenuAfficherContacts(moduleDistribution.Contacts); }),
    TItemMenu("Ajouter un contact",
              []() { MenuAjouterContact(moduleDistribution.Contacts); }),
    TItemMenu("Supprimer un contact",
              []() { MenuSupprimerContact(moduleDistribution.Contacts); }),
    TItemMenu("Afficher l'horaire de distribution",
              []() {
                  MenuAfficherHoraireDistribution(moduleDistribution.Horaire());
              }),
    TItemMenu("Modifier l'horaire de distribution",
              []() {
                  MenuModifierHoraireDistribution(moduleDistribution.Horaire());
              }),
    TItemMenu("Afficher l'heure du système", MenuAfficherHeureSysteme),
    TItemMenu("Changer l'heure du système", MenuChangerHeureSysteme),
};
//! Nombre d'items dans le menu.
const size_t NB_ITEMS_MENU = sizeof(ITEMS_MENU) / sizeof(TItemMenu);

/**
 * @brief Notifie le module de contrôle de la distribution d'un médicament et
 * retourne s'il a accepté qu'un médicament soit distribué
 *
 * @return Si un médicament peut être distribué
 * @retval true Le module de contrôle à accepté qu'un médicament soit distribué
 * @retval false La communication avec le module de contrôle a échouée ou il a
 * refusé de distribué un médicament
 */
bool EnvoyerNbMedic(
    //! Nombre de médicaments à distribuer
    uint8_t nbMedic) {
    return communication.ChangerNbMedicaments(nbMedic) == 0;
}

/**
 * @brief Déverrouille le module de distribution pendant le nb de secondes
 * données
 *
 * @return Un code d'erreur
 * @see TCommunication::DeverrouillerRecharge
 */
int DeverrouillerRecharge(
    //! Le nombre de secondes de déverrouillage
    uint8_t sec) {
    return communication.DeverrouillerRecharge(sec);
}

void setup() {
    Serial.begin(9600);

    TGestionnaireDels::begin(1000);
    TBuzzer::begin(D2, A0, 500);

#if ACTIVER_CONN_CELLULAIRE
    connCell.begin();
#endif

    eeprom.begin();
    moduleDistribution.begin(gestionnaireEEPROM);
    moduleDistribution.EnvoyerNbMedic = EnvoyerNbMedic;
    moduleDistribution.DeverrouillerRecharge = DeverrouillerRecharge;
    communication.begin();

    capteur.begin();

    AfficherMenu(ITEMS_MENU, NB_ITEMS_MENU);
}

void loop() {
    moduleDistribution.loop();

#if ACTIVER_CONN_CELLULAIRE
    connCell.loop();
#endif

    int erreur = capteur.AuthentifierEmpreinteRapidement();
    if (erreur != FINGERPRINT_NOFINGER) {
        if (erreur != 0) {
            Serial.printlnf("Erreur d'authentification d'empreinte: %i",
                            erreur);
            delErreur.Del().FaireClignotter();
        } else {
            Serial.printlnf("Empreinte #%i détectée avec une confiance de %i",
                            capteur.fingerID + 1, capteur.confidence);
            if (!moduleDistribution.Authentifier()) {
                delErreur.Del().FaireClignotter();
                Serial.println("L'authentification n'était pas nécessaire");
            }
        }
    }

    // Gérer le menu.
    GererMenu(ITEMS_MENU, NB_ITEMS_MENU);
}
